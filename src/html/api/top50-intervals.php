<?php

use Db\InvestTechTop50Repository;
use Services\InvestTechService;

require_once("../../platform_config.php");

$repo = InvestTechTop50Repository::getInstance();
$sell_stocks = $repo->getAll(false);
$buy_stocks = $repo->getAll(true);
if ($buy_stocks && $sell_stocks) {
  $sell_to_buy = InvestTechService::getListIntervals($sell_stocks, $buy_stocks);
} else {
  $sell_to_buy = [];
}

echo json_encode($sell_to_buy);
