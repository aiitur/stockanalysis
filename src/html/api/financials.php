<?php

use Db\SimplyWstRepository;
use Db\StockRepository;
use Model\StockData;

require_once("../../platform_config.php");

$buy_stock_data = getTopStockFinancials(true);
$sell_stock_data = getTopStockFinancials(false);

$results = [
  "buy_stocks" => $buy_stock_data,
  "sell_stocks" => $sell_stock_data
];

echo json_encode($results);



/**
 * Get financials of InvestTech Top50 stocks
 * @param bool $is_buy When true, look at buy candidates. When false: sell candidates.
 * @return array
 */
function getTopStockFinancials($is_buy) {
  $repo = StockRepository::getInstance();
  $swstrepo = SimplyWstRepository::getInstance();
  $stocks = $repo->getInvestTechTopStocks($is_buy, "2020-01-03");
  $results = [];
  /** @var StockData $stock */
  foreach ($stocks as $stock) {
    if ($stock->simplywst_ticker) {
      $raw_data = $swstrepo->getRawData($stock->simplywst_ticker);
      if ($raw_data) {
        $financials = extractFinancials($raw_data);
        if ($financials) {
          $results[] = $financials;
        }
      }
    }
  }
  return $results;
}

function extractFinancials(&$raw_data) {
  if ($raw_data && isset($raw_data->data->analysis->data)) {
    $analysis = $raw_data->data->analysis->data;
    return [
      "symbol" => $raw_data->data->unique_symbol,
      "P_E" => $analysis->pe,
      "ROA" => $analysis->roa,
      "ROE" => $analysis->roe,
      "ROCE" => $analysis->extended->data->analysis->past->return_on_capital_employed * 100
    ];
  } else {
    return [];
  }
}


