<?php

namespace Model;

/**
 * Information about a stock crawled from Simply WallSt page
 */
class SimplyWstStockData extends StockData {
  /** @var string */
  public $info_url;
  /** @var string */
  public $company_id;
  /** @var int */
  public $trading_item_id;
  /** @var string */
  public $description;
  /** @var Industry */
  public $industry;
  /** @var string */
  public $raw_json;

  /**
   * Constructor
   * @param string $ticker
   * @param string $exchange_symbol Symbol of the stock, including exchange code. For example, OB:TEL.
   * @param string $name
   * @param int $exchange_id ID of the exchange
   * @param string $info_url
   * @param string $company_id
   * @param int $trading_item_id
   * @param string $description
   * @param Industry $industry
   */
  public function __construct(string $ticker, string $exchange_symbol, string $name, int $exchange_id, $info_url, $company_id,
                              $trading_item_id, $description, $industry) {
    parent::__construct($ticker, $exchange_symbol, $name, $exchange_id);
    $this->info_url = $info_url;
    $this->company_id = $company_id;
    $this->trading_item_id = $trading_item_id;
    $this->description = $description;
    $this->industry = $industry;
  }

}