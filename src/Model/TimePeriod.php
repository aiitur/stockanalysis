<?php

namespace Model;

use Services\TimeService;

class TimePeriod {
  /** @var int */
  public $fromTime;
  /** @var int */
  public $untilTime;

  /**
   * PricePeriod constructor.
   * @param int $fromTime timestamp of the beginning boundary, can be null
   * @param int $untilTime timestamp of the end boundary, can be null
   */
  public function __construct($fromTime = null, $untilTime = null) {
    $this->fromTime = $fromTime;
    $this->untilTime = $untilTime;
  }

  /**
   * Extend the boundaries of the time period by given timestamp
   * @param int $timestamp
   */
  public function extend($timestamp) {
    if ($this->fromTime == null || $this->fromTime > $timestamp) {
      $this->fromTime = $timestamp;
    }
    if ($this->untilTime == null || $this->untilTime < $timestamp) {
      $this->untilTime = $timestamp;
    }
  }

  /**
   * @return int min boundary timestamp
   */
  public function getFromTime() {
    return $this->fromTime;
  }

  /**
   * @return int max boundary timestamp
   */
  public function getUntilTime() {
    return $this->untilTime;
  }

  public function __toString() {
    $t1 = TimeService::timestampToDateTime($this->fromTime)->format("Y-m-d H:i:s");
    $t2 = TimeService::timestampToDateTime($this->untilTime)->format("Y-m-d H:i:s");
    return "Period [$t1, $t2 (UTC)]";
  }


}