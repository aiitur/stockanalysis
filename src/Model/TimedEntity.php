<?php

namespace Model;

use DateTime;

/**
 * Interface TimedEntity
 * An entity that has time
 */
interface TimedEntity {
  /**
   * Get time of the entity
   * @return DateTime
   */
  public function getTime();

  /**
   * Return millisecond-accuracy timestamp, as a string
   * @return string
   */
  public function getMillisecondTime();

  /**
   * Return timestamp that is not rounded up (in case the timestamp had millisecond accuracy)
   * @return int
   */
  public function getFlooredTime();
}