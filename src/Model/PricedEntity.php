<?php

namespace Model;

/**
 * Interface TimedEntity
 * An entity that has a price
 */
interface PricedEntity {
  /**
   * Get default price of the entity
   * @return string decimal value, stored as string
   */
  public function getPrice();

  /**
   * Get opening price of the entity
   * @return string decimal value, stored as string
   */
  public function getOpenPrice();

  /**
   * Get closing price of the entity
   * @return string decimal value, stored as string
   */
  public function getClosePrice();

  /**
   * Get lowest price of the entity
   * @return string decimal value, stored as string
   */
  public function getLowPrice();

  /**
   * Get highest price of the entity
   * @return string decimal value, stored as string
   */
  public function getHighPrice();

  /**
   * Get volume (amount) of the entity
   * @return string decimal value, stored as string
   */
  public function getVolume();
}