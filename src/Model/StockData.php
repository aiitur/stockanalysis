<?php

namespace Model;

/**
 * Base class for storing top stock data
 * Class StockData
 */
class StockData {
  /** @var string */
  public $ticker;
  /** @var string */
  public $exchange_symbol;
  /** @var string */
  public $name;
  /** @var int */
  public $exchange_id;
  /** @var string */
  public $simplywst_ticker;
  /** @var string */
  public $yahoo_ticker;

  /**
   * StockData constructor.
   * @param string $ticker
   * @param string $exchange_symbol Symbol of the stock, including exchange code. For example, OB:TEL.
   * @param string $name
   * @param int $exchange_id ID of the exchange
   */
  public function __construct(string $ticker, string $exchange_symbol, string $name, int $exchange_id) {
    $this->ticker = $ticker;
    $this->exchange_symbol = $exchange_symbol;
    $this->name = $name;
    $this->exchange_id = $exchange_id;
  }

}