<?php

namespace Model;

class InvestTechStockData extends StockData {
  public $score;
  public $price;

  /**
   * Constructor
   * @param string $ticker
   * @param string $exchange_symbol Symbol of the stock, including exchange code. For example, OB:TEL.
   * @param string $name
   * @param float $score
   * @param float $price
   * @param int $exchange_id ID of the exchange
   */
  public function __construct(string $ticker, string $exchange_symbol, string $name, $score, $price, $exchange_id) {
    parent::__construct($ticker, $exchange_symbol, $name, $exchange_id);
    $this->score = $score;
    $this->price = $price;
  }

}