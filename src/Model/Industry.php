<?php

namespace Model;

class Industry {
  /** @var int */
  public $id;
  /** @var string */
  public $name;

  /**
   * Industry constructor.
   * @param int $id
   * @param string $name
   */
  public function __construct(int $id, string $name) {
    $this->id = $id;
    $this->name = $name;
  }

}