<?php

namespace Model;

/**
 * Represents a stock exchange
 */
class Exchange {
  // IDs of exchanges in the database
  const ID_NYSE = 1;
  const ID_OSL = 2;
  const ID_NASDAQ_GS = 3;
  const ID_AMEX = 4;
  const ID_NASDAQ_CM = 5;
  const ID_NASDAQ_GM = 6;
  const OSLO_BORS_PREFIX = "OB:";

  /** @var int */
  public $id;
  public $name;
  public $description;
  public $simplywst_symbol;
  public $yahoo_symbol;
  public $country_code;

  public function __construct(int $id, string $name, string $simplywst_symbol, string $yahoo_symbol,
                              string $country_code, string $description) {
    $this->id = $id;
    $this->name = $name;
    $this->description = $description;
    $this->country_code = $country_code;
    $this->simplywst_symbol = $simplywst_symbol;
    $this->yahoo_symbol = $yahoo_symbol;
  }
}