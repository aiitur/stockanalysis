<?php

namespace Model;
use DateTime;
use Model\PricedEntity;
use Model\TimedEntity;
use Services\NumberService;
use Services\TimeService;

/**
 * Class Price
 * Represents one price candle with High, Low, Open and Close prices.
 * Each price also stores resolution: 1min, 5min, 1h, ...
 * Market and exchange information is not stored here.
 */
class Price implements TimedEntity, PricedEntity {
  // Price states
  const UNKNOWN = 0;
  const REALTIME_STABLE = 1;
  const REALTIME_UNSTABLE = 2;
  const REST_STABLE = 3;
  const EXTERNAL = 4;

  /** @var DateTime */
  public $time;
  /** @var string */
  public $open;
  /** @var string */
  public $close;
  /** @var string */
  public $high;
  /** @var string */
  public $low;
  /** @var string */
  public $volume;
  /** @var string Buy volume divided by total volume. */
  public $buy_ratio;
  /** @var int Resolution in minutes */
  public $resolution;

  /**
   * Price constructor.
   * @param DateTime|string|int $time
   * @param string $open
   * @param string $close
   * @param string $high
   * @param string $low
   * @param string $volume
   * @param int $resolution Resolution, in minutes
   */
  public function __construct($time, $open, $close, $high, $low, $volume, $resolution) {
    $this->time = TimeService::anyToTimestamp($time);
    $this->open = $open;
    $this->close = $close;
    $this->high = $high;
    $this->low = $low;
    $this->volume = $volume;
    $this->resolution = $resolution;
  }

  /**
   * Get time of the entity
   * @return DateTime
   */
  public function getTime() {
    return $this->time;
  }

  /**
   * Return timestamp that is not rounded up (in case the timestamp had millisecond accuracy)
   * @return int
   */
  public function getFlooredTime() {
    // the timestamp is always rounded already, just return it
    return $this->time->getTimestamp();
  }

  /**
   * Return timestamp of the last time moment that this candle covers.
   * For example, for 1h candle it will be the open time + 59min 59sec
   * @return int
   */
  public function getCloseTime() {
    return $this->time->getTimestamp() + $this->resolution * 60 - 1;
  }

  /**
   * Return millisecond-accuracy timestamp, as a string
   * @return string
   */
  public function getMillisecondTime() {
    return "" . $this->time->getTimestamp() . "000";
  }

  public function __toString() {
    $r = "Price [O: $this->open L: $this->low H: $this->high C: $this->close]";
    if ($this->time) {
      $r .= ", " . $this->time->format("Y-m-d H:i:s");
    }
    return $r;
  }


  /**
   * Get price of the entity - close price is the "default" price
   * @return string decimal value, stored as string
   */
  public function getPrice() {
    return $this->close;
  }

  /**
   * Get volume (amount) of the entity
   * @return string decimal value, stored as string
   */
  public function getVolume() {
    return $this->volume;
  }

  /**
   * Get opening price of the entity
   * @return string decimal value, stored as string
   */
  public function getOpenPrice() {
    return $this->open;
  }

  /**
   * Get closing price of the entity
   * @return string decimal value, stored as string
   */
  public function getClosePrice() {
    return $this->close;
  }

  /**
   * Get lowest price of the entity
   * @return string decimal value, stored as string
   */
  public function getLowPrice() {
    return $this->low;
  }

  /**
   * Get highest price of the entity
   * @return string decimal value, stored as string
   */
  public function getHighPrice() {
    return $this->high;
  }

  /**
   * Append a new price to the end of this price candle.
   * @param Price $new_price
   */
  public function append(Price $new_price) {
    if ($this->volume != 0) {
      $this->low = NumberService::min($this->low, $new_price->low);
      $this->high = NumberService::max($this->high, $new_price->high);
      $this->close = $new_price->close;
      $new_volume = bcadd($this->volume, $new_price->volume);
      if ($new_price->buy_ratio && $this->buy_ratio) {
        $buy_vol1 = bcmul($this->volume, $this->buy_ratio);
        $buy_vol2 = bcmul($new_price->volume, $new_price->buy_ratio);
        $buy_vol = bcadd($buy_vol1, $buy_vol2);
        $this->buy_ratio = bcdiv($buy_vol, $new_volume);
      } else {
        // If one of prices has unknown buy ratio, the merged price has unknown ratio as well
        $this->buy_ratio = null;
      }
      $this->volume = $new_volume;
    } else {
      // This candle had zero volume, it was not a real candle, it was merely repetition of the last trade which
      // happened in another candle. When we merge this with the first real candle, reset all the prices
      $this->open = $new_price->open;
      $this->close = $new_price->close;
      $this->high = $new_price->high;
      $this->low = $new_price->low;
      $this->volume = $new_price->volume;
      $this->buy_ratio = $new_price->buy_ratio;
    }
  }
}
