<?php

namespace Model;

class MagicFormulaStockData extends StockData {
  /** @var float */
  public $market_cap;
  /** @var string */
  public $price_date;
  /** @var string */
  public $quarter_date;

  /**
   * MagicFormulaStockData constructor.
   * @param string $ticker
   * @param string $exchange_symbol Symbol of the stock, including exchange code. For example, OB:TEL.
   * @param string $name
   * @param float $market_cap
   * @param string $price_date
   * @param string $quarter_date
   * @param int $exchange_id ID of the exchange
   */
  public function __construct(string $ticker, string $exchange_symbol, string $name, float $market_cap, string $price_date, string $quarter_date,
                              int $exchange_id) {
    parent::__construct($ticker, $exchange_symbol, $name, $exchange_id);
    $this->market_cap = $market_cap;
    $this->price_date = $price_date;
    $this->quarter_date = $quarter_date;
  }
}