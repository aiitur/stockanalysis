<?php

namespace Crawlers;

use Utils\RankedStockList;

/**
 * Crawler for InvestTech pages
 */
class InvestTechCrawler extends AbstractCrawler {
  const URL_FRONTPAGE = "https://www.investtech.com/no/market.php?CountryID=1&product=0";
  const URL_LOGINFORM = "https://www.investtech.com/no/market.php?CountryID=1&product=0";
  const URL_BUY_TABLE = "https://www.investtech.com/no/market.php?MarketID=1&product=41&top=1";
  const URL_SELL_TABLE = "https://www.investtech.com/no/market.php?MarketID=1&product=41&top=0";
  const REFERER = "https://www.investtech.com/no/market.php?CountryID=1&product=0";

  private $email;
  private $password;

  /**
   * InvestTechCrawler constructor.
   * @param string $user_email Email for the InvestTech user, used as a username
   * @param string $password Password for the InvestTech user
   */
  public function __construct($user_email, $password) {
    parent::__construct();
    $this->email = $user_email;
    $this->password = $password;
  }

  /**
   * Returns a list of Top 50 stocks from the table or false on error
   * @return array|bool with twp StockTable objects: Top 50 buys and Top 50 sells, or false on error
   */
  public function crawlTop50Tables() {
    $buy_table = $this->crawlSingleTop50Page(true, true);
    if ($buy_table) {
      sleep(3); // Make sure we always sleep between the two table requests
      $sell_table = $this->crawlSingleTop50Page(false, false);
      $result = [$buy_table, $sell_table];
    } else {
      $result = false;
    }
    return $result;
  }

  /**
   * Fetch HTML page containing the Top50 table
   * @param bool $get_buy When true, get the top 50 buy suggestions. When false, get top 50 sells.
   * @return false|string The HTML content of the page, or false on error
   */
  private function fetchTop50TablePage(bool $get_buy) {
    $url = $get_buy ? self::URL_BUY_TABLE : self::URL_SELL_TABLE;
    echo "Getting TOP 50 table from $url";
    $response = $this->requester->sendGet($url, self::REFERER);
    echo "DONE\n";
    return $response;
  }

  /**
   * Take HTML of the top50 table as input, parse it, extract Top50 table as the output
   * @param string $html The HTML content of the top50 page
   * @param string $filename_prefix Prefix to use for cache file name
   * @return RankedStockList|bool The Top50 table or false on error
   */
  private function parseTop50Table(string &$html, $filename_prefix) {
    try {
      $date = new \DateTime();
    } catch (\Exception $e) {
    }
    $cache_file_name = "{$filename_prefix}_" . date("Y-m-d") . ".html";
    $this->storeFileOnDisk($cache_file_name, $html);
    if (strpos($html, "top50Table") === false) {
      return false; // Page does not contain the top50 table
    }
    $parser = new InvestTechTableParser();
    return $parser->parse($html, $date);
  }

  /**
   * Try to log in by sending HTTP POST with login data. Checks whether the login was successful.
   * @return bool True on success, false on error
   */
  private function tryLogin() {
    $login_form_data = [
      "LOGIN" => $this->email,
      "PASSWORD" => $this->password,
      "LOGIN_REQUEST" => 1,
      "CHECK_LOGIN_MESSAGE" => 1,
      "ajaxLogin" => 1
    ];

    echo "Sending login request...";
    $response = $this->requester->sendPost(self::URL_LOGINFORM, self::URL_FRONTPAGE, $login_form_data);
    echo "DONE\n";
    $this->storeFileOnDisk("login_result.html", $response);
    return $response ? $this->isLoginSuccess($response) : false;
  }

  /**
   * Takes HTML body response from login POST, checks if login was successful
   * @param string $response
   * @return bool
   */
  private function isLoginSuccess(string &$response) {
    return strpos($response, "Feil brukernavn") === false;
  }

  /**
   * Fetch and parse a HTML site containing Top50 table
   * @param bool $is_buy
   * @param bool $try_login When true, try to log in if necessary
   * @return bool|RankedStockList Parsed table or false on error
   */
  public function crawlSingleTop50Page(bool $is_buy, bool $try_login) {
    $page = $this->fetchTop50TablePage($is_buy);
    $fname_prefix = $is_buy ? "buy" : "sell";
    $table = false;
    $error = false;
    if ($page) {
      $table = $this->parseTop50Table($page, $fname_prefix);
      if (!$table) {
        // Session expired, retry to log in
        if ($try_login && $this->tryLogin()) {
          $page = $this->fetchTop50TablePage($is_buy);
          if ($page) {
            $table = $this->parseTop50Table($page, $fname_prefix);
            if (!$table) {
              $error = "Logged in, but $fname_prefix table is not there, something wrong!";
            }
          } else {
            $error = "Logged in, but could not get top 50 $fname_prefix page, something wrong!";
          }
        } else {
          $error = "Could not log in, something wrong!";
        }
      }
    } else {
      $error = "Could not get top 50 page: " . $this->requester->getLastError();
    }

    if ($error) {
      $this->notifyAdmin($error);
    }

    return $table;
  }

}
