<?php

namespace Crawlers;

use Model\Exchange;
use Model\Industry;
use Model\SimplyWstStockData;

class SimplyWstCrawler extends AbstractCrawler {
  const URL_STOCK_LIST = "https://api.simplywall.st/api/grid/filter?include=info,score";
  const URL_RAW_DATA_BASE = "https://api.simplywall.st/api/company%info_url%?include=info%2Cscore%2Cscore.snowflake%2Canalysis.extended.raw_data%2Canalysis.extended.raw_data.insider_transactions&version=2.0";
  const URL_REFERER_RAW_DATA = "https://simplywall.st%info_url%";

  /** @var bool */
  private $include_secondary;

  /**
   * SimplyWstCrawler constructor.
   * @param bool $use_cookies When true, store cookies automatically
   * @param bool $include_secondary When true, include secondary listings (Oslo Mercury, etc)
   */
  public function __construct(bool $use_cookies = true, bool $include_secondary = false) {
    parent::__construct($use_cookies);
    $this->include_secondary = $include_secondary;
  }

  /**
   * Request stocks from Simply WallSt API
   * @param Exchange $exchange
   * @param int $offset Offset in the stock pagination
   * @return array|bool An array ["stocks" => list of SimpleWstStockData objects, "count" => total number of stocks in the list]. or false on error
   */
  public function requestStocks(Exchange $exchange, int $offset) {
    $exchange_symbol = strtolower($exchange->simplywst_symbol);
    if ($this->include_secondary) {
      $primary_flag = "";
    } else {
      $primary_flag = ",[\"primary_flag\",\"=\",true]"; // An optional parameter to show only primary companies
    }
    $post_params = [
      "id" => "0",
      "no_result_if_limit" => false,
      "offset" => $offset,
      "size" => 16,
      "state" => "create",
      "rules" => "[[\"order_by\",\"market_cap\",\"desc\"],[\"value_score\",\">=\",0],[\"dividends_score\",\">=\",0],[\"future_performance_score\",\">=\",0],[\"health_score\",\">=\",0],[\"past_performance_score\",\">=\",0],[\"grid_visible_flag\",\"=\",true],[\"market_cap\",\"is_not_null\"]{$primary_flag},[\"is_fund\",\"=\",false],[\"aor\",[[\"exchange_symbol\",\"in\",[\"$exchange_symbol\"]]]]]"
    ];
    $post_body = json_encode($post_params);
    $country_code = strtolower($exchange->country_code);
    $referer = "https://simplywall.st/screener/create/{$country_code}/any";
    $response = $this->requester->sendPost(self::URL_STOCK_LIST, $referer, $post_body, true);
    return $this->parseStockList($response, $exchange->id);
  }

  /**
   * Parse JSON response containing a list of stocks
   * @param string $response
   * @param int $exchange_id
   * @return array|bool An array ["stocks" => listOfStocks, "count" => totalNumberOfStocksInTheList], or false on error
   */
  private function parseStockList(string $response, int $exchange_id) {
    if (!$response) return [];
    $json = json_decode($response);

    if (!$json || !isset($json->data) || !isset($json->meta->total_records)) {
      echo "Error while parsing stock list response:\n";
      var_dump($json);
      return [];
    }

    $stocks = [];
    foreach ($json->data as $data) {
      $ind = $data->info->data->industry;
      if ($ind && $ind->primary_id && $ind->name) {
        $industry = new Industry($ind->primary_id, $ind->name);
      } else {
        $industry = null;
        echo "No industry for stock $data->ticker_symbol \n";
      }
      $stock = new SimplyWstStockData($data->ticker_symbol, $data->unique_symbol, $data->name, $exchange_id, $data->canonical_url,
        $data->company_id, $data->trading_item_id, $data->info->data->description, $industry);
      $stock->simplywst_ticker = $data->unique_symbol;
      $stock->yahoo_ticker = $data->ticker_symbol;
      $stock->raw_json = $response;
      $stocks[] = $stock;
    }

    return ["stocks" => $stocks, "count" => $json->meta->total_records];
  }

  /**
   * Get raw data for a single stock from the SimplyWall.St API, return the whole response as a string
   * @param SimplyWstStockData $stock
   * @return string
   */
  public function getRawDataFor(SimplyWstStockData $stock) {
    $url = str_replace("%info_url%", $stock->info_url, self::URL_RAW_DATA_BASE);
    $referer = str_replace("%info_url%", $stock->info_url, self::URL_REFERER_RAW_DATA);
    echo "Sending HTTP GET to $url\n";
    return $this->requester->sendGet($url, $referer);
  }
}