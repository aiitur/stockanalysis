<?php

namespace Crawlers;

use Services\HttpRequester;

class AbstractCrawler {
  const COOKIE_FILE_PATH = "./cookies.txt";
  const DEBUG_FILE_FOLDER = "./debug_output";

  protected $requester;

  /**
   * AbstractCrawler constructor.
   * @param bool $use_cookies When true, store cookies automatically
   */
  public function __construct(bool $use_cookies = true) {
    $this->requester = new HttpRequester($use_cookies ? self::COOKIE_FILE_PATH : null);
  }

  public function __destruct() {
    $this->requester->closeSession();
  }

  protected function notifyAdmin($error) {
    echo "ERROR: $error\n";
    // TODO - send email to admin
  }

  /**
   * Store a file in the debug file folder
   * @param string $filename Relative filename (without path)
   * @param string $file_content Content of the file
   */
  protected function storeFileOnDisk(string $filename, string &$file_content) {
    $filepath = self::DEBUG_FILE_FOLDER . "/$filename";
    file_put_contents($filepath, $file_content);
  }

}