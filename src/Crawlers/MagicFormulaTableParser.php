<?php

namespace Crawlers;

use Db\StockRepository;
use Model\Exchange;
use Model\MagicFormulaStockData;
use Utils\RankedStockList;

/**
 * Parses response from Magic formula HTML page
 */
class MagicFormulaTableParser {
  const NAME_COL_INDEX = 0;
  const TICKER_COL_INDEX = 1;
  const MARKET_CAP_COL_INDEX = 2;
  const PRICE_DATE_INDEX = 3;
  const FINANCIALS_DATE_INDEX = 4;

  /**
   * Take HTML of the top50 table as input, parse it, extract Top50 table as the output
   * @param string $html The HTML content of the top50 page
   * @param \DateTime $date Date when this table was fetched from MagicFormulaInvesting.com
   * @return RankedStockList|bool The Top50 table or false on error
   */
  public function parse(string &$html, \DateTime $date) {
    $doc = new \DOMDocument();
    if (!@$doc->loadHTML($html)) {
      echo "Error while parsing the HTML file!\n";
      return false;
    }

    $xpath = new \DOMXPath($doc);
    // Select all rows except the title row
    $rows = $xpath->query('//table[contains(@class, \'screeningdata\')]/tbody/tr');
    $table = new RankedStockList($date);
    $stock_repo = StockRepository::getInstance();
    $nr = 1;
    /** @var \DOMElement $row */
    foreach ($rows as $row) {
      $cells = $row->getElementsByTagName("td");
      $name = $cells->item(self::NAME_COL_INDEX)->nodeValue;
      $ticker = $cells->item(self::TICKER_COL_INDEX)->nodeValue;
      $market_cap_str = $cells->item(self::MARKET_CAP_COL_INDEX)->nodeValue;
      $market_cap = (float) str_replace(",", "", $market_cap_str); // Remove thousand-separating commas
      $price_date = $cells->item(self::PRICE_DATE_INDEX)->nodeValue;
      $quarter_date = $cells->item(self::FINANCIALS_DATE_INDEX)->nodeValue;
      // Not all USA stocks are in NYSE, find the right code
      $stock = $stock_repo->getByTickerCountry($ticker, "us");
      if ($stock) {
        $stock_data = new MagicFormulaStockData($ticker, $stock->exchange_symbol, $name, $market_cap, $price_date,
          $quarter_date, Exchange::ID_NYSE);
        $table->add($nr, $stock_data);
        $nr++;
      } else {
        echo "Could not find stock $ticker in the Database, ignoring it from magic table!\n";
      }
    }

    return $table;
  }
}