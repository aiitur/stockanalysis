<?php

namespace Crawlers;

use Model\Exchange;
use Model\InvestTechStockData;
use Utils\RankedStockList;

/**
 * Handles parsing a Top50 table from HTML
 */
class InvestTechTableParser {
  const NR_COL_INDEX = 0;
  const NAME_COL_INDEX = 1;
  const TICKER_COL_INDEX = 2;
  const SCORE_COL_INDEX = 3;
  const PRICE_COL_INDEX = 4;

  /**
   * Take HTML of the top50 table as input, parse it, extract Top50 table as the output
   * @param string $html The HTML content of the top50 page
   * @param \DateTime $date Date when this table was fetched from InvestTech
   * @return RankedStockList|bool The Top50 table or false on error
   */
  public function parse(string &$html, \DateTime $date) {
    $doc = new \DOMDocument();
    if (!@$doc->loadHTML($html)) {
      echo "Error while parsing the HTML file!\n";
      return false;
    }

    $xpath = new \DOMXPath($doc);
    // Select all rows except the title row
    $rows = $xpath->query('//table[@class="top50Table"]/tr[position()>1]');
    $table = new RankedStockList($date);
    /** @var \DOMElement $row */
    $expected_nr = 1;
    foreach ($rows as $row) {
      $cells = $row->getElementsByTagName("td");
      $nr = (int)$cells->item(self::NR_COL_INDEX)->nodeValue;
      $name = $cells->item(self::NAME_COL_INDEX)->nodeValue;
      $ticker = $cells->item(self::TICKER_COL_INDEX)->nodeValue;
      $score = (float)$cells->item(self::SCORE_COL_INDEX)->nodeValue;
      $price = (float)$cells->item(self::PRICE_COL_INDEX)->nodeValue;
      if ($nr != $expected_nr) {
        echo "Something wrong while parsing row Nr $expected_nr : $nr $name $ticker $score $price\n";
        $table = false;
        break;
      }
      $exchange_symbol = $this->getExchangeSymbol($ticker);
      $stock_data = new InvestTechStockData($ticker, $exchange_symbol, $name, $score, $price, Exchange::ID_OSL);
      $stock_data->yahoo_ticker = $this->getYahooTicker($ticker);
      $table->add($nr, $stock_data);
      $expected_nr++;
    }

    return $table;
  }

  /**
   * Get exchange symbol for a ticker
   * @param string $ticker TEL, FB, etc
   * @return string OB:TEL, NasdaqGS:FB, etc
   */
  private function getExchangeSymbol(string $ticker) {
    // Some tickers are exceptions
    switch ($ticker) {
      case "TEONE": return "OM:TEONE";
      default: return Exchange::OSLO_BORS_PREFIX . $ticker;
    }
  }

  /**
   * Get a ticker used in Yahoo Finance
   * @param string $ticker stock ticker: PARB, KID, etc
   * @return string Yahoo ticker: PARB.OL, etc
   */
  private function getYahooTicker(string $ticker) {
    return $ticker . ".OL";
  }
}