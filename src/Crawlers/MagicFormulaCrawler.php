<?php

namespace Crawlers;

use Utils\RankedStockList;

/**
 * Crawls MagicFormulaInvesting.com page
 */
class MagicFormulaCrawler extends AbstractCrawler {
  const URL = "https://www.magicformulainvesting.com/Screening/StockScreening";
  const URL_LOGIN = "https://www.magicformulainvesting.com/Account/LogOn";
  const URL_LOGIN_REFER = "https://www.magicformulainvesting.com";

  private $email;
  private $password;

  /**
   * MagicFormulaCrawler constructor.
   * @param string $email Email used to log in to magicformulainvesting.com
   * @param string $password Password used to log in to magicformulainvesting.com
   */
  public function __construct($email, $password) {
    parent::__construct();
    $this->email = $email;
    $this->password = $password;
  }


  /**
   * Returns a list of Top 50 stocks from the table or false on error
   * @param bool $select_top30 When true, select top 30 companies. When false, select top 50.
   * @return RankedStockList with Top 50 buys, or false on error
   */
  public function crawlTopTable(bool $select_top30) {
    $page = $this->fetchTopTablePage($select_top30);
    $table = false;
    $error = false;
    if ($this->requester->wasRedirected() && strpos($this->requester->getRedirectUrl(), self::URL_LOGIN) !== false) {
      // We were redirected to login page
      if ($this->tryLogin()) {
        $page = $this->fetchTopTablePage($select_top30);
      } else {
        $page = false;
      }
    }

    if ($page) {
      $table = $this->parseTopTable($page);
      if (!$table) {
        $error = "Logged in, but magicformula table is not there!";
      }
    } else {
      $error = "Could not get top 50 magic formula page: " . $this->requester->getLastError();
    }

    if ($error) {
      $this->notifyAdmin($error);
    }

    return $table;

  }

  /**
   * Fetch the HTML page containing the top 50 magic formula investing entries
   * @param bool $select_top30 When true, select top 30 companies. When false, select top 50.
   * @return false|string HTML content of the page, false on error
   */
  private function fetchTopTablePage(bool $select_top30) {
    echo "Posting TOP 50 magic formula form to: " . self::URL;
    $top30 = $select_top30 ? "true" : "false";
    $post_data = [
      "MinimumMarketCap" => "50",
      "Select30" => $top30,
      "stocks" => "Get Stocks"
    ];
    $response = $this->requester->sendPost(self::URL, self::URL, $post_data);
    echo "DONE\n";
    return $response;
  }

  /**
   * Try to log in by sending HTTP POST with login data. Checks whether the login was successful.
   * @return bool True on success, false on error
   */
  private function tryLogin() {
    $login_form_data = [
      "Email" => $this->email,
      "Password" => $this->password,
      "login" => "Login",
    ];

    echo "Sending login request...";
    $response = $this->requester->sendPost(self::URL_LOGIN, self::URL_LOGIN_REFER, $login_form_data);
    echo "DONE\n";
    $this->storeFileOnDisk("login_result.html", $response);
    return $response ? $this->isLoginSuccess($response) : false;

  }

  /**
   * Takes HTML body response from login POST, checks if login was successful
   * @param string $response
   * @return bool
   */
  private function isLoginSuccess(string $response) {
    return strpos($response, "Object moved to <a href=\"/Screening/StockScreening\">here</a>") !== false;
  }

  /**
   * Take HTML of the top50 table as input, parse it, extract Top50 table as the output
   * @param string $html The HTML content of the top50 page
   * @return RankedStockList|bool The Top50 table or false on error
   */
  private function parseTopTable(string $html) {
    try {
      $date = new \DateTime();
    } catch (\Exception $e) {
    }
    $cache_file_name = "magic_formula_" . date("Y-m-d") . ".html";
    $this->storeFileOnDisk($cache_file_name, $html);
    if (strpos($html, "screeningdata") === false) {
      return false; // Page does not contain the top50 table
    }
    $parser = new MagicFormulaTableParser();
    return $parser->parse($html, $date);
  }
}