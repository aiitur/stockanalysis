<?php

namespace Tasks;

/**
 * Waits a random delay before running the real tasl
 */
abstract class DelayedRunner extends Runner {
  protected $min_delay = 10;
  protected $max_delay = 100;

  /**
   * @inheritDoc
   */
  public function start($arguments) {
    // Check arguments first. If arguments are not ok, no need to waste time sleeping
    if (!$this->argumentsOk($arguments)) {
      echo "Argument fail, aborting script\n";
      return false;
    }

    if (!DEV) {
      $sleep_time = rand($this->min_delay, $this->max_delay);
      echo get_class() . " sleeping $sleep_time seconds...\n";
      sleep($sleep_time);
    } else {
      echo "Avoiding sleep in DEV environment\n";
    }
    return $this->realStart($arguments);
  }

  /**
   * Start the real operation
   * @param mixed $arguments
   * @return bool True on success, false otherwise
   */
  protected abstract function realStart($arguments);

  /**
   * Check arguments before running the process
   * @param mixed $arguments
   * @return bool True if arguments are ok, false otherwise
   */
  protected abstract function argumentsOk($arguments);
}