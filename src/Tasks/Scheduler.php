<?php

namespace Tasks;

use Db\TaskRepository;

/**
 * Task scheduler
 */
class Scheduler {
  public function run() {
    echo "Starting task scheduler at " . date("Y-m-d H:i:s") . "\n";
    if ($this->finishPreviousTasks()) {
      $this->startNextPendingTask();
    }
  }

  /**
   * Check if there is a task currently running, abort it if it has taken too much time.
   * @return bool True when ready to start next task, false otherwise.
   */
  private function finishPreviousTasks() {
    $repo = TaskRepository::getInstance();
    $task = $repo->getTaskInProgress();
    if ($task) {
      if ($task->hasSpentAllTime()) {
        return $this->abortTask($task);
      } else {
        echo "Task $task->id still in progress, wait before scheduling new tasks\n";
        return false;
      }
    } else {
      return true;
    }
  }

  /**
   * Find next task, try to start it
   */
  private function startNextPendingTask() {
    $repo = TaskRepository::getInstance();
    $task = $repo->getNextPendingTask();
    if ($task) {
      echo "Starting task $task->id\n";
      $this->updateTaskState($task, Task::ST_SCHEDULED);
      if ($this->executeProcess($task)) {
        $this->updateTaskState($task, Task::ST_FINISHED);
        echo "Task $task->id finished\n";
      } else {
        echo "FAILED to start task $task->id\n";
      }
    }
  }

  /**
   * Update state for the task and store it in DB
   * @param Task $task
   * @param int $new_state
   * @return bool True on success, false otherwise
   */
  private function updateTaskState(Task $task, int $new_state) {
    $task->setState($new_state);
    $repo = TaskRepository::getInstance();
    return $repo->update($task);
  }

  /**
   * Execute the given task
   * @param Task $task
   * @return bool True when process successfully finished, false otherwise
   */
  private function executeProcess(Task $task) {
    $this->updateTaskState($task, Task::ST_STARTING);
    $class = $this->findClass($task->getRunnerClass());
    if ($class) {
      /** @var Runner $runner */
      $runner = new $class();
      $task->setPid(getmypid());
      $this->updateTaskState($task, Task::ST_RUNNING);
      echo "Running task $task->id class = $class\n";
      $runner->start($task->arguments);
      return true;
    } else {
      $task->error = "Class not found";
      $this->updateTaskState($task, Task::ST_FAILED);
      echo "Could not find runner for task $task->id, class = " . $task->getRunnerClass() . "\n";
      return false;
    }
  }

  /**
   * Try to abort a task
   * @param Task $task
   * @return bool True when task successfully terminated, false on error.
   */
  private function abortTask($task) {
    echo "Task $task->id has taken too much time, aborting it\n";
    $pid = $task->getPid();
    $aborted = false;
    if ($pid) {
      if ($this->killProcess($pid)) {
        $task->error = "Time is up";
        $aborted = true;
      } else if ($this->processExists($pid)) {
        echo "Could not abort task $task->id with PID $pid\n";
      } else {
        $task->error = "Process disappeared, could not abort";
        echo "Could not kill process $pid for task $task->id, but it did not exist either (problem solved?)\n";
        $aborted = true;
      }
    } else {
      echo "Task $task->id has no PID\n";
    }

    if ($aborted) {
      $this->updateTaskState($task, Task::ST_ABORTED);
    }

    return $aborted;
  }

  /**
   * Try to kill a process with given process ID
   * @param int $pid Process ID
   * @return bool True on success, false otherwise.
   */
  private function killProcess($pid) {
    if (function_exists('posix_kill')) {
      return posix_kill($pid, SIGTERM);
    } elseif (function_exists('exec') && strstr(PHP_OS, 'WIN')) {
      return exec("taskkill /F /PID $pid") ? true : false;
    } else {
      return false;
    }
  }

  /**
   * Try to find and load a Runner class
   * @param string $class_name
   * @return string|null The class name, including namespace; or null if class not found
   */
  private function findClass(string $class_name) {
    $class = __NAMESPACE__ . "\\" . $class_name;
    if (\Autoloader::loader($class)) {
      return $class;
    } else {
      return null;
    }
  }

  /**
   * Return true if a process with specific ID is running
   * @param int $pid Process ID
   * @return bool
   */
  private function processExists(int $pid) {
    $exists = false;
    if (function_exists('posix_kill')) {
      // Unix systems
      $exists = posix_kill($pid, 0);
    } elseif (function_exists('exec') && strstr(PHP_OS, 'WIN')) {
      // Windows
      $task_list = [];
      exec("tasklist /fo csv 2>NUL", $task_list);
      // Remove the first line, it contains headers
      array_shift($task_list);
      // Take each line, split it into parts, find the PID
      foreach ($task_list as $task_info) {
        // Replace all huge spaces by double spaces
        $task_parts = explode("\",\"", $task_info);
        if (count($task_parts) == 5) {
          $pid_info = (int)$task_parts[1];
          if ($pid_info == $pid) {
            $exists = true;
            break;
          }
        }
      }
    }

    return $exists;
  }
}