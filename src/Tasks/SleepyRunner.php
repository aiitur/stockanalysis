<?php

namespace Tasks;

/**
 * A task runner that sleeps for a very long time (used for testing of task abortion)
 */
class SleepyRunner extends Runner {
  /**
   * @inheritDoc
   */
  public function start($arguments) {
    echo get_class() . " running\n";
    sleep(10000);
    return true;
  }
}