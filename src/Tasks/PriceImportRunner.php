<?php

//////////////////////////////////////////////////////////
/// Fetch price candles, import them in the database
//////////////////////////////////////////////////////////

namespace Tasks;

use Db\PriceRepository;

/**
 * Gathers detailed info about one particular stock from SimplyWall.St
 */
class PriceImportRunner extends DelayedStockRunner {
  const PERIOD = "3mo";

  public function __construct() {
    $this->max_delay = 70;
  }

  private function storePrices($prices) {
    $repo = PriceRepository::getInstance();
    foreach ($prices as $price) {
      $timestamp = $price->Date / 1000; // Convert from milliseconds to seconds
      $repo->addPrice($this->stock->exchange_symbol, $timestamp, $price->Open, $price->Close, $price->Low, $price->High, $price->Volume);
    }
  }

  /**
   * @inheritDoc
   */
  protected function realStart($arguments) {
    // make sure we are in this directory to get path to the script correctly
    chdir(dirname(__FILE__));

    $period = self::PERIOD;
    $ticker = $this->stock->yahoo_ticker;
    $script = "python3 ../apps/fetch-candles.py \"$ticker\" \"$period\"";
    $price_json = exec($script);
    $success = false;
    if ($price_json) {
      $prices = json_decode($price_json);
      if ($prices && !is_string($prices)) {
        $this->storePrices($prices);
        $success = true;
      }
    }

    return $success;
  }
}