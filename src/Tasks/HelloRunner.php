<?php

namespace Tasks;

class HelloRunner extends Runner {

  /**
   * @inheritDoc
   */
  public function start($arguments) {
    echo "HelloRunner running\n";
    if ($arguments) {
      echo "Arguments:\n";
      var_dump($arguments);
    }
    return true;
  }
}