<?php

namespace Tasks;

use Crawlers\SimplyWstCrawler;
use Db\SimplyWstRepository;
use Model\SimplyWstStockData;

/**
 * Runner that takes in symbol of a particular stock as an argument
 */
abstract class DelayedStockRunner extends DelayedRunner {
  /** @var string */
  protected $symbol;
  /** @var SimplyWstStockData */
  protected $stock;

  /**
   * @inheritDoc
   */
  protected function argumentsOk($arguments) {
    $this->symbol = $arguments;
    if ($this->symbol) {
      $repo = SimplyWstRepository::getInstance();
      $this->stock = $repo->get($this->symbol);
      if ($this->stock) {
        echo "Will process $this->symbol \n";
      } else {
        echo "Can not find simplywst_stock data for symbol $this->symbol \n";
      }
    } else {
      echo "Error: exchange_symbol not set for " . get_class() . "\n";
    }

    return $this->stock != null;
  }
}