<?php

namespace Tasks;

use Crawlers\SimplyWstCrawler;
use Db\SimplyWstRepository;

/**
 * Gathers detailed info about one particular stock from SimplyWall.St
 */
class SWStStockCrawlRunner extends DelayedStockRunner {
  /**
   * @inheritDoc
   */
  public function realStart($arguments) {
    $crawler = new SimplyWstCrawler(false, false);
    echo "Get raw data from SimplWall.St for stock $this->symbol\n";
    $raw_data = $crawler->getRawDataFor($this->stock);
    $success = false;
    if ($raw_data) {
      $repo = SimplyWstRepository::getInstance();
      if ($repo->saveRawData($this->stock->simplywst_ticker, $raw_data)) {
        echo "Raw data saved for $this->symbol\n";
        $success = true;
      } else {
        echo "Could not save raw data for $this->symbol\n";
      }
    } else {
      echo "Could not get raw data for stock $this->symbol\n";
    }

    return $success;
  }
}