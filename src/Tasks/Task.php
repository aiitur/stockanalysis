<?php

namespace Tasks;

/**
 * Represents a task to be executed by the task scheduler
 */
class Task {
  // Valid task states
  const ST_NEW = 1; // Task is waiting in the queue
  const ST_SCHEDULED = 2; // Task is taken by the scheduler, will be started
  const ST_STARTING = 3; // Starting a new process, not sure yet whether it will run successfully
  const ST_RUNNING = 4; // Task is currently running
  const ST_FINISHED = 5; // Task was successfully finished
  const ST_FINISHED_WITH_ERROR = 6; // Task process exited with an error
  const ST_ABORTED = 7; // Task was started but had to be aborted before it was done with work
  const ST_FAILED = 8; // Could not start the task

  /** @var int */
  public $id;
  /** @var string Runner class that will execute this task */
  private $runner_class;
  /** @var int ID of process executing this task */
  private $pid;
  /** @var int use one of predefined ST_xxx values */
  private $state;
  /** @var \DateTime when the task was created (written in the DB) */
  private $time_created;
  /** @var \DateTime when a process was started to execute the process */
  private $time_started;
  /** @var \DateTime when the process finished */
  private $time_finished;
  /** @var string Resulting error message of the process. Null on success. */
  public $error;
  /** @var int maximum allowed time for running, in seconds */
  public $max_allowed_time;
  /** @var string Arguments passed to the runner's run() method */
  public $arguments;

  /**
   * Task constructor.
   * @param int $id
   * @param string $runner_class Runner class that will execute this task
   * @param int $pid
   * @param int $state
   * @param string $error
   */
  public function __construct(int $id, string $runner_class, $pid, int $state, $error) {
    $this->id = $id;
    $this->runner_class = $runner_class;
    $this->pid = $pid;
    $this->state = $state;
    $this->error = $error;
  }

  /**
   * @return string The Runner class that should execute this task
   */
  public function getRunnerClass() {
    return $this->runner_class;
  }

  /**
   * Returns ID of the process running the task
   * @return int
   */
  public function getPid() {
    return $this->pid;
  }

  /**
   * Return true if the given state signals start of a process
   * @param int $state
   * @return bool
   */
  private static function isStartState(int $state) {
    return $state == self::ST_SCHEDULED || $state == self::ST_STARTING;
  }

  /**
   * Return true if the given state signals end of a process (with either success or failure)
   * @param int $state
   * @return bool
   */
  public static function isFinishState(int $state) {
    switch ($state) {
      case self::ST_FINISHED:
      case self::ST_FAILED:
      case self::ST_ABORTED:
      case self::ST_FINISHED_WITH_ERROR:
        return true;
      default:
        return false;
    }
  }

  /**
   * @return int
   */
  public function getState() {
    return $this->state;
  }

  /**
   * Set state of the task.
   * @param int $state Must be one of predefined constants ST_xxx
   */
  public function setState(int $state) {
    $this->state = $state;
    try {
      if (self::isStartState($state)) {
        $this->time_started = new \DateTime();
      } else if (self::isFinishState($state)) {
        $this->time_finished = new \DateTime();
      }
    } catch (\Exception $e) {
      echo "Error while updating state for task $this->id: " . $e->getMessage() . "\n";
      echo $e->getTrace() . "\n";
    }
  }

  /**
   * Set time of creation from a timestamp
   * @param int $timestamp When not-null, use it as a timestamp. When null, set the time property to null
   */
  public function initCreationTime($timestamp) {
    if ($timestamp) {
      try {
        $this->time_created = new \DateTime();
        $this->time_created->setTimestamp($timestamp);
      } catch (\Exception $e) {
        $this->time_created = null;
      }
    } else {
      $this->time_created = null;
    }
  }

  /**
   * Set time of start from a timestamp
   * @param int $timestamp When not-null, use it as a timestamp. When null, set the time property to null
   */
  public function initStartTime($timestamp) {
    if ($timestamp) {
      try {
        $this->time_started = new \DateTime();
        $this->time_started->setTimestamp($timestamp);
      } catch (\Exception $e) {
        $this->time_started = null;
      }
    } else {
      $this->time_started = null;
    }
  }

  /**
   * Set time of finish from a timestamp
   * @param int $timestamp When not-null, use it as a timestamp. When null, set the time property to null
   */
  public function initFinishTime($timestamp) {
    if ($timestamp) {
      try {
        $this->time_finished = new \DateTime();
        $this->time_finished->setTimestamp($timestamp);
      } catch (\Exception $e) {
        $this->time_finished = null;
      }
    } else {
      $this->time_finished = null;
    }
  }

  /**
   * Return timestamp of start time
   * @return int|null Timestamp or null if start time is unknown.
   */
  public function getStartTimestamp() {
    return $this->time_started ? $this->time_started->getTimestamp() : null;
  }

  /**
   * Return timestamp of finish time
   * @return int|null Timestamp or null if finish time is unknown.
   */
  public function getFinishTimestamp() {
    return $this->time_finished ? $this->time_finished->getTimestamp() : null;
  }

  /**
   * Returns true if this task has already spent all the allowed time.
   * @return bool
   */
  public function hasSpentAllTime() {
    if ($this->max_allowed_time) {
      if ($this->time_started) {
        $latest_end_time = $this->time_started->getTimestamp() + $this->max_allowed_time;
        return time() > $latest_end_time;
      } else {
        // Task not started yet
        return false;
      }
    } else {
      // No time limit
      return false;
    }
  }

  /**
   * Set the process ID for the task. This can't be done when the process is already in progress or terminated
   * @param int $pid
   */
  public function setPid(int $pid) {
    $this->pid = $pid;
  }

}