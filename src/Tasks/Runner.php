<?php


namespace Tasks;

/**
 * Base class for all task executors. To create your own task handler, create a child class that extends this one.
 */
abstract class Runner {
  /**
   * Execute a task
   * @param mixed $arguments Optional arguments passed to the running process
   * @return bool True when process finished successfully, false otherwise
   */
  public abstract function start($arguments);
}