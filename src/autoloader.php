<?php

/**
 * This is autoloader class that will include all classes under libraries
 * folder.
 */

// Make sure our working dir is where the script is located

class Autoloader {
  static public function loader($className) {
    $scriptDir = dirname(__FILE__);
    
    // Determine only class name.
    $path = explode('\\', $className);
    
    $class_to_include = array_pop($path);

    $filename = $scriptDir.DIRECTORY_SEPARATOR.str_replace("\\", DIRECTORY_SEPARATOR, $className) . ".php";

    //echo "Try to load: ".$filename."\n";
    
    if (file_exists($filename)) {
      require_once($filename);
    }
    
    // Experimental exception - to not fail it class exists even not loaded.
    if (!class_exists($className)) {
      if (!interface_exists($className)) {
        return FALSE;
      } else {
        return TRUE;
      }
    }
    
    return TRUE;
  }
}
spl_autoload_register('Autoloader::loader');

