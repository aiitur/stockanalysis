<?php

////////////////////////////////////////////////////////////////////////////////
// APP: Tries to crawl a HTML page containing top 50 buy and sell suggestions. Logs in if necessary
////////////////////////////////////////////////////////////////////////////////

// Make sure we are in the "current directory"
use Crawlers\InvestTechCrawler;
use Db\InvestTechTop50Repository;
use Utils\RankedStockList;

chdir(__DIR__);

require_once("../platform_config.php");

echo "InvestTech crawler starting at " . date("Y-m-d H:i:s") . "\n";

if ((int) date("N") > 5 && !DEV) {
  echo "It is weekend, skipping the crawling\n";
  return;
}

// Don't send the request at the same time every day
if (!DEV) {
  $random_sleep_seconds = rand(45, 20 * 60);
  echo "Sleeping $random_sleep_seconds seconds...\n";
  sleep($random_sleep_seconds);
  echo "DONE sleeping, starting crawling at " . date("Y-m-d H:i:s");
} else {
  echo "No sleeping in dev environment\n";
}

$crawler = new InvestTechCrawler(INVESTTECH_EMAIL, INVESTTECH_PASSWORD);
/** @var RankedStockList $buy_table */
/** @var RankedStockList $sell_table */
$res = $crawler->crawlTop50Tables();
if ($res !== false) {
  [$buy_table, $sell_table] = $res;
  saveTableToDb($buy_table, true, "BUY");
  saveTableToDb($sell_table, false, "SELL");
} else {
  echo "Could not crawl the sites";
}


function saveTableToDb(RankedStockList $table, bool $is_buy, string $title) {
  $success = true;
  if ($table->getSize() == 50) {
    $dbTable = InvestTechTop50Repository::getInstance();
    if (!$dbTable->save($table, $is_buy)) {
      echo "Error while saving Top 50 $title entries to DB\n";
      $success = false;
    }
  } else {
    echo "Something wrong, $title table does not contain 50 entries!\n";
    $success = false;
  }
  return $success;
}
