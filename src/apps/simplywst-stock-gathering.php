<?php

///////////////////////////////////////////////////////////////////////////////////////
// APP: Gathers basic info about all stocks in a specific country from Simplywall.st
///////////////////////////////////////////////////////////////////////////////////////

// Make sure we are in the "current directory"
use Crawlers\SimplyWstCrawler;
use Db\ExchangeRepository;
use Db\SimplyWstRepository;
use Db\StockRepository;
use Model\Exchange;
use Model\SimplyWstStockData;

chdir(__DIR__);

require_once("../platform_config.php");

//////////////////////////////////////////////////////
/// Config
//////////////////////////////////////////////////////
$exchange_id = Exchange::ID_OSL;
//////////////////////////////////////////////////////


$exchange_repo = new ExchangeRepository();
$exchange = $exchange_repo->getById($exchange_id);
if (!$exchange) {
  echo "Could not find exchange in DB!";
  return;
}

$crawler = new SimplyWstCrawler(false, false);

$stock_repo = StockRepository::getInstance();
$existing_stocks = $stock_repo->getAll();

$stocks = [];
$offset = 0; // Offset in the pagination
$num_stocks = 1000000; // Total number of stocks in the exchange (unknown now, we set it to a huge number)

while ($offset < $num_stocks) {
  echo "Requesting stocks with offset $offset \n";
  $ret = $crawler->requestStocks($exchange, $offset);
  if ($ret) {
    if ($num_stocks != $ret["count"]) {
      $num_stocks = $ret["count"];
      echo "Total stock count: $num_stocks\n";
    }
    $stocks = $ret["stocks"];
    storeStockData($stocks, $existing_stocks);
  } else {
    echo "Error while requesting Simply WSt stocks for offset $offset!";
    $num_stocks = 0;
  }
  $offset += count($stocks);
  echo "$offset stocks processed\n";
  sleep(rand(3, 5));
}


/**
 * Store stock data in the database
 * @param array $stocks List of SimplyWstStockData objects
 * @param array $existing_stocks Array of stocks already found in the DB, keys are tickers.
 */
function storeStockData(array $stocks, array & $existing_stocks) {
  $stock_repo = StockRepository::getInstance();
  $swst_repo = SimplyWstRepository::getInstance();

  /** @var SimplyWstStockData $stock */
  foreach ($stocks as $stock) {
    if (!isset($existing_stocks[$stock->exchange_symbol])) {
      $stock_repo->add($stock);
    } else {
      // Stock already registered, update exchange ID and other fields
      $stock_repo->update($stock);
    }
    $rc = $swst_repo->add($stock);
    if ($rc == 1) {
      echo "ADDED SWSt data for stock $stock->name\n";
    } else if ($rc == 0) {
      echo "SWSt data for stock $stock->name was already in the DB\n";
    } else {
      echo "Error: could not add swst data to DB: \n";
      var_dump($stock);
    }
  }
}


