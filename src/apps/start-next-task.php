<?php

///////////////////////////////////////////////////////////////////
/// Take the next pending task from the queue, try to start it
///////////////////////////////////////////////////////////////////

chdir(__DIR__);

use Tasks\Scheduler;

require_once("../platform_config.php");

$scheduler = new Scheduler();
$scheduler->run();
