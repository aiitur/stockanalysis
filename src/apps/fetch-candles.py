"""
Fetch historical candles for a specific stock, return as a json string
Expected command line parameters:
[symbol] [period]
"""

import yfinance as yf
import sys


def main():
    if len(sys.argv) != 3:
        print("\"Must specify symbol and period as command line arguments!\"")
        exit()

    symbol = sys.argv[1]
    period = sys.argv[2]

    ticker = yf.Ticker(symbol)

    prices = ticker.history(period=period)
    # Add another index to include Date as a value
    prices = prices.reset_index()
    print(prices.to_json(orient="records", index=True))


if __name__ == "__main__":
    main()
