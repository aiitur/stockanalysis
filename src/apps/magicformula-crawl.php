<?php

////////////////////////////////////////////////////////////////////////////////
// APP: Tries to crawl a HTML page containing top 50 suggestions of magic formula. Logs in if necessary
// https://www.magicformulainvesting.com/Screening/StockScreening
////////////////////////////////////////////////////////////////////////////////

// Make sure we are in the "current directory"
use Crawlers\MagicFormulaCrawler;
use Db\MagicFormulaRepository;
use Services\MagicFormulaService;
use Utils\RankedStockList;

chdir(__DIR__);

require_once("../platform_config.php");

echo "MagicFormula crawler starting at " . date("Y-m-d H:i:s") . "\n";

if (!DEV && (int) date("N") > 5) {
  echo "It is weekend, skipping the crawling\n";
  return;
}

// Don't send the request at the same time every day
if (!DEV) {
  $random_sleep_seconds = rand(45, 20 * 60);
  echo "Sleeping $random_sleep_seconds seconds...\n";
  sleep($random_sleep_seconds);
  echo "DONE sleeping, starting crawling at " . date("Y-m-d H:i:s");
} else {
  echo "No sleeping in dev environment\n";
}

$crawler = new MagicFormulaCrawler(MAGIC_FORMULA_EMAIL, MAGIC_FORMULA_PASSWORD);
/** @var RankedStockList $buy_table */
$top30_table = $crawler->crawlTopTable(true);
if ($top30_table) {
  $top50_table = $crawler->crawlTopTable(false);
  if ($top50_table) {
    $repo = new MagicFormulaRepository();
    $merged_top_table = MagicFormulaService::mergeTables($top30_table, $top50_table);
    if (!$repo->save($merged_top_table)) {
      echo "Error while saving Top 50 magic formula entries to DB\n";
      $success = false;
    }
  } else {
    echo "Could not crawl the magic formula top 50 table";
  }
} else {
  echo "Could not crawl the magic formula top 30 table";
}
