<?php

// Create backup of the database, store it in /backups directory

require_once("platform_config.php");

$fname = "db-backup_" . date("Y-m-d_His");
$dbname = DB_NAME;
$pwd = DB_PWD;
$host = DB_HOST;
$port = DB_PORT;

echo "Starting backup at " . date("Y-m-d H:i:s") . "\n";
$cmd = "mysqldump -P {$port} -p{$pwd} -h {$host} {$dbname}  > /backups/{$dbname}.sql && zip /backups/{$fname}.zip /backups/{$dbname}.sql";
exec($cmd);
echo "DB backup done at " . date("Y-m-d H:i:s") . "\n";

