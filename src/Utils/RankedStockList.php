<?php

namespace Utils;

use DateTime;
use Model\StockData;

/**
 * Contains a list of StockData entries, organized by ranks (indices)
 */
class RankedStockList {
  /** @var array */
  private $entries;
  /** @var DateTime */
  private $date;

  /**
   * StockTable constructor.
   * @param DateTime|string $date Date when this table was observed.
   */
  public function __construct($date) {
    if (is_string($date)) {
      try {
        $date = new DateTime($date);
      } catch (\Exception $e) {
        $date = null;
      }
    }
    $this->entries = [];
    $this->date = $date;
  }


  /**
   * Add an entry to the table
   * @param int $nr Number of entry in the table
   * @param StockData $stock_data Entry with stock data
   */
  public function add(int $nr, StockData $stock_data) {
    $this->entries[$nr] = $stock_data;
  }

  /**
   * Return the number of entries currently in the table
   * @return int
   */
  public function getSize() {
    return count($this->entries);
  }

  /**
   * Get the entries currently in the table. Use key to get position in the table.
   * @return array Array with StockData objects
   */
  public function getEntries() {
    return $this->entries;
  }

  public function getDate() {
    return $this->date;
  }
}