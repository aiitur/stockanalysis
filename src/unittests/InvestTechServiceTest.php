<?php

use Model\InvestTechStockData;
use Model\StockData;
use PHPUnit\Framework\TestCase;
use Services\InvestTechService;
use Utils\RankedStockList;

require_once("../platform_config.php");

class InvestTechServiceTest extends TestCase {

  public function testListIntervals_empty() {
    $sell = [];
    $buy = [];
    $res = InvestTechService::getListIntervals($sell, $buy);
    $this->assertEquals([], $res);
  }

  //////////////////////////////////////////////////////////////////////////
  // A stock only once in sell list
  //////////////////////////////////////////////////////////////////////////
  public function testListIntervals_oneSell() {
    $pareto_ticker = "PARB";
    $exchange_symbol = "OB:PARB";
    $pareto = new InvestTechStockData($pareto_ticker, $exchange_symbol, "Pareto bank", -80, 38, 2);

    $sl = new RankedStockList(new DateTime("2020-01-01"));
    $sl->add(1, $pareto);
    $sell_list = [
      "2020-01-01" => $sl
    ];
    $buy_list = [];
    $expected = [
      $exchange_symbol => [
        "stock" => $pareto->name,
        "intervals" => [
          [
            "dates" => ["2020-01-01"],
            "list" => 1
          ]
        ]
      ]
    ];
    $intervals = InvestTechService::getListIntervals($sell_list, $buy_list);
    $this->assertEquals($expected, $intervals);
  }

  ///////////////////////////////////////////////``///////////////////////////
  // A stock only in buy list
  //////////////////////////////////////////////////////////////////////////
  public function testListIntervals_oneBuy() {
    $pareto_ticker = "PARB";
    $exchange_symbol = "OB:PARB";
    $pareto = new InvestTechStockData($pareto_ticker, $exchange_symbol, "Pareto bank", -80, 38, 2);
    $bl = new RankedStockList(new DateTime("2020-01-01"));
    $bl->add(1, $pareto);
    $buy_list = [
      "2020-01-01" => $bl
    ];
    $sell_list = [];
    $expected = [
      $exchange_symbol => [
        "stock" => $pareto->name,
        "intervals" => [
          [
            "dates" => ["2020-01-01"],
            "list" => 2
          ]
        ]
      ]
    ];
    $intervals = InvestTechService::getListIntervals($sell_list, $buy_list);
    $this->assertEquals($expected, $intervals);
  }

  //////////////////////////////////////////////////////////////////////////
  // A stock in sell list for several days
  //////////////////////////////////////////////////////////////////////////
  public function testListIntervals_oneSellInterval() {
    $pareto_ticker = "PARB";
    $exchange_symbol = "OB:PARB";
    $pareto = new InvestTechStockData($pareto_ticker, $exchange_symbol, "Pareto bank", -80, 38, 2);
    $sl1 = new RankedStockList(new DateTime("2020-01-01"));
    $sl1->add(1, $pareto);
    $sl2 = new RankedStockList(new DateTime("2020-01-02"));
    $sl2->add(5, $pareto);
    $sl3 = new RankedStockList(new DateTime("2020-01-03"));
    $sl3->add(15, $pareto);
    $sell_list = [
      "2020-01-01" => $sl1,
      "2020-01-02" => $sl2,
      "2020-01-03" => $sl3
    ];
    $buy_list = [];
    $expected = [
      $exchange_symbol => [
        "stock" => $pareto->name,
        "intervals" => [
          [
            "dates" => ["2020-01-01", "2020-01-02", "2020-01-03"],
            "list" => 1
          ]
        ]
      ]
    ];
    $intervals = InvestTechService::getListIntervals($sell_list, $buy_list);
    $this->assertEquals($expected, $intervals);
  }

  //////////////////////////////////////////////////////////////////////////
  // A stock in both lists: sell first, then buy
  //////////////////////////////////////////////////////////////////////////
  public function testListIntervals_sellIntBuyInt() {
    $pareto_ticker = "PARB";
    $exchange_symbol = "OB:PARB";
    $buy_pareto = new InvestTechStockData($pareto_ticker, $exchange_symbol, "Pareto bank", 80, 30, 2);
    $sell_pareto = new InvestTechStockData($pareto_ticker, $exchange_symbol, "Pareto bank", -80, 38, 2);
    $sl1 = new RankedStockList(new DateTime("2020-01-01"));
    $sl1->add(1, $sell_pareto);
    $sl2 = new RankedStockList(new DateTime("2020-01-02"));
    $sl2->add(5, $sell_pareto);
    $sl3 = new RankedStockList(new DateTime("2020-01-03"));
    $sl3->add(45, $sell_pareto);
    $sell_list = [
      "2020-01-01" => $sl1,
      "2020-01-02" => $sl2,
      "2020-01-03" => $sl3
    ];
    $bl1 = new RankedStockList(new DateTime("2020-01-05"));
    $bl1->add(42, $buy_pareto);
    $bl2 = new RankedStockList(new DateTime("2020-01-07"));
    $bl2->add(12, $buy_pareto);
    $buy_list = [
      "2020-01-05" => $bl1,
      "2020-01-07" => $bl2,
    ];
    $expected = [
      $exchange_symbol => [
        "stock" => $buy_pareto->name,
        "intervals" => [
          [
            "dates" => ["2020-01-01", "2020-01-02", "2020-01-03"],
            "list" => 1
          ],
          [
            "dates" => ["2020-01-05", "2020-01-07"],
            "list" => 2
          ]
        ]
      ]
    ];
    $intervals = InvestTechService::getListIntervals($sell_list, $buy_list);
    $this->assertEquals($expected, $intervals);
  }

  //////////////////////////////////////////////////////////////////////////
  // A stock in both lists: buy first, then sell
  //////////////////////////////////////////////////////////////////////////
  public function testListIntervals_buyIntSellInt() {
    $pareto_ticker = "PARB";
    $exchange_symbol = "OB:PARB";
    $buy_pareto = new InvestTechStockData($pareto_ticker, $exchange_symbol, "Pareto bank", 80, 30, 2);
    $sell_pareto = new InvestTechStockData($pareto_ticker, $exchange_symbol, "Pareto bank", -80, 38, 2);
    $sl1 = new RankedStockList(new DateTime("2020-01-11"));
    $sl1->add(1, $sell_pareto);
    $sl2 = new RankedStockList(new DateTime("2020-01-12"));
    $sl2->add(5, $sell_pareto);
    $sl3 = new RankedStockList(new DateTime("2020-01-13"));
    $sl3->add(45, $sell_pareto);
    $sell_list = [
      "2020-01-11" => $sl1,
      "2020-01-12" => $sl2,
      "2020-01-13" => $sl3
    ];
    $bl1 = new RankedStockList(new DateTime("2020-01-05"));
    $bl1->add(42, $buy_pareto);
    $bl2 = new RankedStockList(new DateTime("2020-01-07"));
    $bl2->add(12, $buy_pareto);
    $buy_list = [
      "2020-01-05" => $bl1,
      "2020-01-07" => $bl2,
    ];
    $expected = [
      $exchange_symbol => [
        "stock" => $buy_pareto->name,
        "intervals" => [
          [
            "dates" => ["2020-01-05", "2020-01-07"],
            "list" => 2
          ],
          [
          "dates" => ["2020-01-11", "2020-01-12", "2020-01-13"],
          "list" => 1
        ],
        ]
      ]
    ];
    $intervals = InvestTechService::getListIntervals($sell_list, $buy_list);
    $this->assertEquals($expected, $intervals);
  }

  //////////////////////////////////////////////////////////////////////////
  // A stock in buy list, another stock in sell list with same dates
  //////////////////////////////////////////////////////////////////////////
  public function testListIntervals_twoStocksSameDates() {
    $pareto_ticker = "PARB";
    $exchange_symbol = "OB:PARB";
    $another_symbol = "OB:Another";
    $another_name = "Another stock";
    $stock1 = new InvestTechStockData($pareto_ticker, $exchange_symbol, "Pareto bank", -80, 38, 2);
    $stock2 = new InvestTechStockData("Another", $another_symbol, $another_name, 80, 30, 2);
    $sl1 = new RankedStockList(new DateTime("2020-01-11"));
    $sl1->add(1, $stock1);
    $sl2 = new RankedStockList(new DateTime("2020-01-12"));
    $sl2->add(5, $stock1);
    $sell_list = [
      "2020-01-11" => $sl1,
      "2020-01-12" => $sl2,
    ];
    $bl0 = new RankedStockList(new DateTime("2020-01-11"));
    $bl0->add(12, $stock2);
    $bl1 = new RankedStockList(new DateTime("2020-01-12"));
    $bl1->add(42, $stock2);
    $buy_list = [
      "2020-01-11" => $bl0,
      "2020-01-12" => $bl1,
    ];
    $expected = [
      $exchange_symbol => [
        "stock" => $stock1->name,
        "intervals" => [
          [
            "dates" => ["2020-01-11", "2020-01-12"],
            "list" => 1
          ],
        ]
      ],
      $another_symbol => [
        "stock" => $another_name,
        "intervals" => [
          [
            "dates" => ["2020-01-11", "2020-01-12"],
            "list" => 2
          ],
        ]
      ],
    ];
    $intervals = InvestTechService::getListIntervals($sell_list, $buy_list);
    $this->assertEquals($expected, $intervals);
  }

  //////////////////////////////////////////////////////////////////////////
  // Stock changes lists: 3 days in list1, 2 dates not in any list (another stock appears there), then appears in list2
  //////////////////////////////////////////////////////////////////////////
  public function testListIntervals_buyIntZeroSellInt() {
    $pareto_ticker = "PARB";
    $exchange_symbol = "OB:PARB";
    $another_symbol = "OB:Another";
    $another_name = "Another stock";
    $buy_pareto = new InvestTechStockData($pareto_ticker, $exchange_symbol, "Pareto bank", 80, 30, 2);
    $sell_pareto = new InvestTechStockData($pareto_ticker, $exchange_symbol, "Pareto bank", -80, 38, 2);
    $stock2 = new InvestTechStockData("Another", $another_symbol, $another_name, 80, 30, 2);
    $sl1 = new RankedStockList(new DateTime("2020-01-11"));
    $sl1->add(1, $sell_pareto);
    $sl2 = new RankedStockList(new DateTime("2020-01-12"));
    $sl2->add(5, $sell_pareto);
    $sl3 = new RankedStockList(new DateTime("2020-01-13"));
    $sl3->add(45, $sell_pareto);
    $sell_list = [
      "2020-01-11" => $sl1,
      "2020-01-12" => $sl2,
      "2020-01-13" => $sl3
    ];
    $bl0 = new RankedStockList(new DateTime("2020-01-14"));
    $bl0->add(12, $stock2);
    $bl1 = new RankedStockList(new DateTime("2020-01-15"));
    $bl1->add(42, $buy_pareto);
    $bl2 = new RankedStockList(new DateTime("2020-01-17"));
    $bl2->add(12, $buy_pareto);
    $buy_list = [
      "2020-01-14" => $bl0,
      "2020-01-15" => $bl1,
      "2020-01-17" => $bl2,
    ];
    $expected = [
      $exchange_symbol => [
        "stock" => $buy_pareto->name,
        "intervals" => [
          [
            "dates" => ["2020-01-11", "2020-01-12", "2020-01-13"],
            "list" => 1
          ],
          [
            "dates" => ["2020-01-15", "2020-01-17"],
            "list" => 2
          ],
        ]
      ],
      $another_symbol => [
        "stock" => $another_name,
        "intervals" => [
          [
            "dates" => ["2020-01-14"],
            "list" => 2
          ],
        ]
      ],
    ];
    $intervals = InvestTechService::getListIntervals($sell_list, $buy_list);
    $this->assertEquals($expected, $intervals);
  }

  //////////////////////////////////////////////////////////////////////////
  // A stock goes from list1 to list2, then back to list1
  //////////////////////////////////////////////////////////////////////////
  public function testListIntervals_buySellBuy() {
    $pareto_ticker = "PARB";
    $exchange_symbol = "OB:PARB";
    $buy_pareto = new InvestTechStockData($pareto_ticker, $exchange_symbol, "Pareto bank", 80, 30, 2);
    $sell_pareto = new InvestTechStockData($pareto_ticker, $exchange_symbol, "Pareto bank", -80, 38, 2);
    $sl1 = new RankedStockList(new DateTime("2020-01-11"));
    $sl1->add(1, $sell_pareto);
    $sl2 = new RankedStockList(new DateTime("2020-01-12"));
    $sl2->add(5, $sell_pareto);
    $sl3 = new RankedStockList(new DateTime("2020-01-17"));
    $sl3->add(45, $sell_pareto);
    $sell_list = [
      "2020-01-11" => $sl1,
      "2020-01-12" => $sl2,
      "2020-01-17" => $sl3
    ];
    $bl1 = new RankedStockList(new DateTime("2020-01-15"));
    $bl1->add(42, $buy_pareto);
    $bl2 = new RankedStockList(new DateTime("2020-01-16"));
    $bl2->add(12, $buy_pareto);
    $buy_list = [
      "2020-01-15" => $bl1,
      "2020-01-16" => $bl2,
    ];
    $expected = [
      $exchange_symbol => [
        "stock" => $buy_pareto->name,
        "intervals" => [
          [
            "dates" => ["2020-01-11", "2020-01-12"],
            "list" => 1
          ],
          [
            "dates" => ["2020-01-15", "2020-01-16"],
            "list" => 2
          ],
          [
            "dates" => ["2020-01-17"],
            "list" => 1
          ],
        ]
      ],
    ];
    $intervals = InvestTechService::getListIntervals($sell_list, $buy_list);
    $this->assertEquals($expected, $intervals);
  }

  //////////////////////////////////////////////////////////////////////////
  // Stock changes lists: 3 days in list1, 2 dates not in any list, then appears in list2, while another stock changes list in the middle
  //////////////////////////////////////////////////////////////////////////
  public function testListIntervals_mixTwochanges() {
    $pareto_ticker = "PARB";
    $exchange_symbol = "OB:PARB";
    $another_symbol = "OB:Another";
    $another_name = "Another stock";
    $stock1_buy = new InvestTechStockData($pareto_ticker, $exchange_symbol, "Pareto bank", 80, 30, 2);
    $stock1_sell = new InvestTechStockData($pareto_ticker, $exchange_symbol, "Pareto bank", -80, 38, 2);
    $stock2_buy = new InvestTechStockData("Another", $another_symbol, $another_name, 80, 30, 2);
    $stock2_sell = new InvestTechStockData("Another", $another_symbol, $another_name, -80, 20, 2);

    // Sell Stock 1
    $sl1 = new RankedStockList(new DateTime("2020-01-11"));
    $sl1->add(1, $stock1_sell);
    $sl2 = new RankedStockList(new DateTime("2020-01-12"));
    $sl2->add(5, $stock1_sell);

    // Buy Stock 2
    $bl1 = new RankedStockList(new DateTime("2020-01-13"));
    $bl1->add(1, $stock2_buy);

    // Sell Stock 2
    $sl3 = new RankedStockList(new DateTime("2020-01-14"));
    $sl3->add(5, $stock2_sell);

    // Buy Stock 1
    $bl2 = new RankedStockList(new DateTime("2020-01-15"));
    $bl2->add(1, $stock1_buy);

    $sell_list = [
      "2020-01-11" => $sl1,
      "2020-01-12" => $sl2,
      "2020-01-14" => $sl3
    ];
    $buy_list = [
      "2020-01-13" => $bl1,
      "2020-01-15" => $bl2,
    ];
    $expected = [
      $stock1_buy->exchange_symbol => [
        "stock" => $stock1_buy->name,
        "intervals" => [
          [
            "dates" => ["2020-01-11", "2020-01-12"],
            "list" => 1
          ],
          [
            "dates" => ["2020-01-15"],
            "list" => 2
          ],
        ]
      ],
      $stock2_buy->exchange_symbol => [
        "stock" => $stock2_buy->name,
        "intervals" => [
          [
            "dates" => ["2020-01-13"],
            "list" => 2
          ],
          [
            "dates" => ["2020-01-14"],
            "list" => 1
          ]
        ]
      ],
    ];
    $intervals = InvestTechService::getListIntervals($sell_list, $buy_list);
    $this->assertEquals($expected, $intervals);
  }

  //////////////////////////////////////////////////////////////////////////
  // Stock changes lists: 3 days in list1, 2 dates not in any list, then appears in list1 again, while another stockis in another list in the middle
  //////////////////////////////////////////////////////////////////////////
  public function testListIntervals_silenceInTheMiddle() {
    $pareto_ticker = "PARB";
    $exchange_symbol = "OB:PARB";
    $another_symbol = "OB:Another";
    $another_name = "Another stock";
    $stock1_buy = new InvestTechStockData($pareto_ticker, $exchange_symbol, "Pareto bank", 80, 30, 2);
    $stock1_sell = new InvestTechStockData($pareto_ticker, $exchange_symbol, "Pareto bank", -80, 38, 2);
    $stock2_buy = new InvestTechStockData("Another", $another_symbol, $another_name, 80, 30, 2);
    $stock2_sell = new InvestTechStockData("Another", $another_symbol, $another_name, -80, 20, 2);

    // Sell Stock 1
    $sl1 = new RankedStockList(new DateTime("2020-01-11"));
    $sl1->add(1, $stock1_sell);
    $sl2 = new RankedStockList(new DateTime("2020-01-12"));
    $sl2->add(5, $stock1_sell);

    // Buy Stock 2
    $bl1 = new RankedStockList(new DateTime("2020-01-13"));
    $bl1->add(1, $stock2_buy);


    // Sell Stock 1 again - should be a new interval
    $sl3 = new RankedStockList(new DateTime("2020-01-15"));
    $sl3->add(1, $stock1_sell);
    $sl4 = new RankedStockList(new DateTime("2020-01-21"));
    $sl4->add(1, $stock1_sell);

    $sell_list = [
      "2020-01-11" => $sl1,
      "2020-01-12" => $sl2,
      "2020-01-15" => $sl3,
      "2020-01-21" => $sl4
    ];
    $buy_list = [
      "2020-01-13" => $bl1,
    ];
    $expected = [
      $stock1_buy->exchange_symbol => [
        "stock" => $stock1_buy->name,
        "intervals" => [
          [
            "dates" => ["2020-01-11", "2020-01-12"],
            "list" => 1
          ],
          [
            "dates" => ["2020-01-15", "2020-01-21"],
            "list" => 1
          ],
        ]
      ],
      $stock2_buy->exchange_symbol => [
        "stock" => $stock2_buy->name,
        "intervals" => [
          [
            "dates" => ["2020-01-13"],
            "list" => 2
          ],
        ]
      ],
    ];
    $intervals = InvestTechService::getListIntervals($sell_list, $buy_list);
    $this->assertEquals($expected, $intervals);
  }
}
