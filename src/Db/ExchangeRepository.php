<?php

namespace Db;

use Model\Exchange;

class ExchangeRepository {

  /**
   * Get exchange by ID
   * @param int $id
   * @return Exchange|null
   */
  public function getById(int $id) {
    $db = DbConn::getInstance();
    if (!$db) return null;

    $query = "SELECT * FROM exchanges WHERE id = ?";
    $params = [$id];
    $row = $db->execSingleRowQuery($query, $params);
    $e = null;
    if ($row) {
      $e = new Exchange($row["id"], $row["name"], $row["simplywst_symbol"], $row["yahoo_symbol"],
        $row["country_code"], $row["description"]);
    }

    return $e;
  }
}