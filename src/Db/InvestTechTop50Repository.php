<?php

namespace Db;

use DateTime;
use Model\InvestTechStockData;
use Utils\RankedStockList;

/**
 * Handles InvestTech Top50 table storage in SQL database
 */
class InvestTechTop50Repository {
  const TABLE_BUY = "investtech_top50_buy";
  const TABLE_SELL = "investtech_top50_sell";

  static private $instance;

  private function __construct() {
  }

  public static function getInstance() {
    if (!self::$instance) {
      self::$instance = new InvestTechTop50Repository();
    }
    return self::$instance;
  }

  /**
   * Save entries from a table to DB
   * @param RankedStockList $table
   * @param bool $is_buy When true, store it as BUY suggestion table. When false, store it as SELL suggestions.
   * @return bool True on success, false on error.
   */
  public function save(RankedStockList $table, bool $is_buy) {
    $db = DbConn::getInstance();
    if (!$db) return false;

    $stockRepo = StockRepository::getInstance();
    $stocks = $stockRepo->getAll();

    $priceRepo = PriceRepository::getInstance();

    $entries = $table->getEntries();
    // Count how many items added to DB
    $entries_added = 0;
    $stocks_added = 0;
    $prices_added = 0;

    /**
     * @var int $rating
     * @var InvestTechStockData $topStock
     */
    foreach ($entries as $rating => $topStock) {
      $symbol = $topStock->exchange_symbol;
      if (!isset($stocks[$symbol])) {
        // This stock is not found in the DB yet, store it
        $a = $stockRepo->add($topStock);
        if ($a < 0) break;
        $stocks_added += $a;
      }
      $close_price = $topStock->price;
      $score = $topStock->score;
      $date = $table->getDate();
      $a = $priceRepo->addPrice($symbol, $date->getTimestamp(), null, $close_price, null, null, null);
      if ($a < 0) break;
      $prices_added += $a;

      $a = $this->add($symbol, $date, $rating, $score, $is_buy);
      if ($a < 0) break;
      $entries_added += $a;
    }

    echo "$stocks_added Stocks added to DB\n";
    echo "$entries_added Top50 entries added to DB\n";
    echo "$prices_added Prices added to DB\n";

    return true;
  }

  /**
   * Save a rating to SQL table
   * @param string $exchange_symbol Symbol of the stock (unique, including the exchange code): OB:EL, NasdaqGS:TSLA, etc.
   * @param DateTime $date Date when this rating was checked
   * @param int $rating InvestTech rating of this stock at the given moment
   * @param float $score InvestTech score for the stock
   * @param bool $is_buy When true, store it as BUY suggestion table. When false, store it as SELL suggestions.
   * @return int 1 when insert was made successfully, 0, when duplicate row already existed in the table, -1 on error
   */
  private function add(string $exchange_symbol, \DateTime $date, int $rating, float $score, bool $is_buy) {
    $db = DbConn::getInstance();
    if (!$db) return -1;

    $table_name = $is_buy ? self::TABLE_BUY : self::TABLE_SELL;

    $query = "INSERT INTO $table_name (ticker, insert_time, rating, score) VALUES (?, FROM_UNIXTIME(?), ?, ?)";
    $params = [$exchange_symbol, $date->getTimestamp(), $rating, $score];
    $row_count = $db->execUpdateQuery($query, $params);
    if ($row_count == 0 && !$db->wasDuplicate()) {
      // Duplicate entry is ok, all other errors are not
      echo "ERROR: " . DbConn::getLastError() . "\n";
      $row_count = -1;
    }
    return $row_count;
  }

  public function getAll($is_buy) {
    $db = DbConn::getInstance();
    if (!$db) return -1;

    $table_name = $is_buy ? self::TABLE_BUY : self::TABLE_SELL;
    $query = "SELECT it.rating, it.score, DATE(it.insert_time) AS insert_date, s.name, s.exchange_symbol, s.ticker, s.exchange_id, dp.closep, s.simplywst_ticker as swst
        FROM $table_name AS it LEFT JOIN stocks s ON s.exchange_symbol = it.ticker
        LEFT JOIN daily_prices dp on s.exchange_symbol = dp.ticker AND DATE(it.insert_time) = dp.date
        ORDER BY insert_time, rating";
    $res = $db->execSelectQuery($query, []);
    $stocks = [];
    if ($res) {
      foreach ($res as $row) {
        $date = $row["insert_date"];
        if (!isset($stocks[$date])) {
          $stocks[$date] = new RankedStockList($date);
        }
        $stock_data = new InvestTechStockData($row["ticker"], $row["exchange_symbol"], $row["name"], $row["score"], $row["closep"], $row["exchange_id"]);
        $stock_data->simplywst_ticker = $row["swst"];
        $stocks[$date]->add($row["rating"], $stock_data);
      }
    }
    return $stocks;
  }
}