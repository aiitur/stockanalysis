<?php

namespace Db;

use Model\StockData;

class StockRepository {
  /**
   * Private constructor for singleton needs.
   */
  private function __construct() {
  }

  private static $instance;

  /**
   * Return a singleton.
   * @return StockRepository
   */
  public static function getInstance() {
    if (!self::$instance) {
      self::$instance = new StockRepository();
    }
    return self::$instance;
  }

  /**
   * Return an array containing Stock objects. Keys in the array will be tickers.
   * @return array Array of stocks, empty array on error
   */
  public function getAll() {
    return $this->selectAll("SELECT * FROM stocks", []);
  }

  /**
   * Select all stocks which have ever appeared on top 50 list (buy or sell candidates) as rated by InvestTech
   * @param bool $is_buy When true, select stocks which have been in the top50 purchase candidate list. When false,
   * look into the sell candidate list.
   * @param string|null $date Specify date when the data was collected from InvestTech. Get all stocks when date not specified
   * @return array list containing StockData objects
   */
  public function getInvestTechTopStocks(bool $is_buy, $date = null) {
    $table = $is_buy ? InvestTechTop50Repository::TABLE_BUY : InvestTechTop50Repository::TABLE_SELL;
    if ($date) {
      $params = [$date];
      $where = "WHERE DATE(insert_time) = ?";
    } else {
      $params = [];
      $where = "";
    }
    $query = "SELECT s.* FROM `stocks` s INNER JOIN $table t ON t.ticker = s.exchange_symbol $where GROUP BY s.exchange_symbol";
    return $this->selectAll($query, $params);
  }

  /**
   * Select all stocks which have ever appeared on top 50 magic formula list
   * @return array list containing StockData objects
   */
  public function getMagicFormulaTopStocks() {
    $table = MagicFormulaRepository::TABLE_NAME;
    $query = "SELECT s.* FROM `stocks` s INNER JOIN $table t ON t.ticker = s.exchange_symbol GROUP BY s.exchange_symbol";
    return $this->selectAll($query, []);
  }

  /**
   * Select all stocks by a given query
   * @param string $query
   * @param array $params
   * @return array
   */
  private function selectAll(string $query, array $params) {
    $db = DbConn::getInstance();
    if (!$db) return [];

    $results = $db->execSelectQuery($query, $params);
    $stocks = [];
    if ($results) {
      foreach ($results as $row) {
        $stocks[$row["exchange_symbol"]] = $this->createFromRow($row);
      }
    }
    return $stocks;
  }

  /**
   * Insert a new row in the stock table
   * @param StockData $stock
   * @return int 1 when insert was made successfully, 0, when duplicate row already existed in the table, -1 on error
   */
  public function add(StockData $stock) {
    $db = DbConn::getInstance();
    if (!$db) return -1;

    $params = [$stock->ticker, $stock->exchange_symbol, $stock->name, $stock->exchange_id];

    $query = "INSERT INTO `stocks` (ticker, exchange_symbol, name, exchange_id #opt_fields#) VALUES (?, ?, ?, ? #opt_vals#)";
    $this->addOptParams($stock, $query, $params, false);
    $row_count = $db->execUpdateQuery($query, $params);
    if ($row_count == 0 && !$db->wasDuplicate()) {
      // Duplicate entry is ok, all other errors are not
      echo "ERROR: " . DbConn::getLastError() . "\n";
      $row_count = -1;
    }
    return $row_count;
  }

  /**
   * Update an existing stock entry in DB, identify it by ticker only (not by exchange ID!)
   * @param StockData $stock
   * @return int Number of rows updated
   */
  public function update(StockData $stock) {
    $db = DbConn::getInstance();
    if (!$db) return -1;

    $params = [$stock->ticker, $stock->name, $stock->exchange_id];
    $query = "UPDATE stocks set ticker = ?, name = ?, exchange_id = ? #opt_fields# WHERE exchange_symbol = ?";
    $this->addOptParams($stock, $query, $params, true);
    $params[] = $stock->exchange_symbol; // This param must be the last
    $row_count = $db->execUpdateQuery($query, $params);
    if ($row_count == 0 && !$db->wasDuplicate()) {
      echo "ERROR: " . DbConn::getLastError() . "\n";
    }

    return $row_count;
  }

  /**
   * Add optional parameters to query variables. Find #opt_fields# and #opt_vals# placeholders in the query
   * @param StockData $stock
   * @param string $query Query to be updated.
   * @param array $params
   * @param bool $is_update When true, treat as an update query instead of insert
   */
  private function addOptParams(StockData $stock, string &$query, array &$params, bool $is_update) {
    $fields = $vals = "";
    if ($stock->simplywst_ticker) {
      if ($is_update) {
        $fields .= ", simplywst_ticker = ?";
      } else {
        $fields .= ", simplywst_ticker";
        $vals .= ", ?";
      }
      $params[] = $stock->simplywst_ticker;
    }
    if ($stock->yahoo_ticker) {
      if ($is_update) {
        $fields .= ", yahoo_ticker = ?";
      } else {
        $fields .= ", yahoo_ticker";
        $vals .= ", ?";
      }
      $params[] = $stock->yahoo_ticker;
    }
    $query = str_replace("#opt_fields#", $fields, $query);
    $query = str_replace("#opt_vals#", $vals, $query);
  }

  /**
   * Find a stock by ticker and country code. Usable in scenarios where we know the ticker but don't know exactly in
   * which exchange the stock is traded.
   * @param string $ticker NOR, TSLA, etc
   * @param string $country_code us, no, etc
   * @return StockData|null
   */
  public function getByTickerCountry(string $ticker, string $country_code) {
    $db = DbConn::getInstance();
    if (!$db) return null;

    $query = "SELECT s.* FROM `stocks` s INNER JOIN exchanges e ON e.id = s.exchange_id WHERE (ticker = ? OR yahoo_ticker = ?) AND e.country_code LIKE ?";
    $params = [$ticker, $ticker, $country_code];
    $row = $db->execSingleRowQuery($query, $params);
    if ($row) {
      $stock = $this->createFromRow($row);
    } else {
      $stock = null;
    }
    return $stock;
  }


  /**
   * Create a Stock object from a row selected from DB.
   * @param array $row
   * @return StockData
   */
  private function createFromRow(array $row) {
    $stock = new StockData($row["ticker"], $row["exchange_symbol"], $row["name"], $row["exchange_id"]);
    $stock->simplywst_ticker = $row["simplywst_ticker"];
    $stock->yahoo_ticker = $row["yahoo_ticker"];
    return $stock;
  }

}