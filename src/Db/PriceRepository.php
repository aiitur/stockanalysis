<?php

namespace Db;

use Model\Price;
use Services\TimeService;

class PriceRepository {
  static $instance;

  private function __construct() {
  }

  /**
   * Use this method to get a singleton reference
   * @return PriceRepository
   */
  public static function getInstance() {
    if (!self::$instance) {
      self::$instance = new PriceRepository();
    }
    return self::$instance;
  }

  /**
   * @param string $ticker
   * @param int $timestamp
   * @param float $open_price
   * @param float $close_price
   * @param float $high_price
   * @param float $low_price
   * @param float $volume
   * @return int 1 when insert was made successfully, 0, when duplicate row already existed in the table, -1 on error
   */
  public function addPrice(string $ticker, int $timestamp, $open_price, $close_price, $low_price, $high_price, $volume) {
    $db = DbConn::getInstance();
    if (!$db) return -1;

    $query = "REPLACE INTO `daily_prices` (ticker, date, openp, closep, lowp, highp, volume) VALUES 
                (?, FROM_UNIXTIME(?), ?, ?, ?, ?, ?)";
    $params = [$ticker, $timestamp, $open_price, $close_price, $low_price, $high_price, $volume];
    $row_count = $db->execUpdateQuery($query, $params);
    if ($row_count == 0 && !$db->wasDuplicate()) {
      // Duplicate entry is ok, all other errors are not
      echo "ERROR: " . DbConn::getLastError() . "\n";
      $row_count = -1;
    }
    return $row_count;
  }

  public function getAllFor(string $ticker) {
    $db = DbConn::getInstance();
    if (!$db) return -1;

    $query = "SELECT *, UNIX_TIMESTAMP(date) AS tstamp FROM `daily_prices`  WHERE ticker = ?";
    $res = $db->execSelectQuery($query, [$ticker]);
    $prices = [];
    if ($res) {
      foreach ($res as $row) {
        $tstamp = (int) $row["tstamp"];
        $date = TimeService::anyToTimestamp($tstamp)->format("Y-m-d");
        $prices[$date] = new Price($tstamp, $row["openp"], $row["closep"], $row["highp"], $row["lowp"], $row["volume"],
          TimeService::SECONDS_IN_DAY / 60);
      }
    }
    return $prices;
  }
}