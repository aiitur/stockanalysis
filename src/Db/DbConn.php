<?php

namespace Db;

/**
 * Class that handles DB connection and executes queries. use getInstance() to get a singleton instance of it
 */
class DbConn {
    /** @var DbConn */
    private static $instance;

    /** @var \mysqli */
    private $conn;
    private $connected;
    private $last_error;

    // Constructor allowed only internally, to implement the singleton access
    private function __construct() {
        $this->last_error = false;
        $this->conn = new \mysqli(DB_HOST, DB_USER, DB_PWD, DB_NAME, DB_PORT);
        $this->connected = !$this->conn->connect_error;
        if (!$this->connected) {
            $this->last_error = $this->conn->error;
        }
    }

    /**
     * Get singleton instance of the DbConn class
     * @return DbConn connection or null if connection failed
     */
    public static function getInstance() {
        if (!self::$instance) {
            self::$instance = new DbConn();
        }
        return self::$instance->connected ? self::$instance : null;
    }

    /**
     * Get the last DB error message
     * @return string|bool Error message or false if not connected to DB
     */
    public static function getLastError() {
        if (self::$instance && self::$instance->connected) {
            return self::$instance->last_error;
        } else {
            return false;
        }
    }

    /**
     * Execute a query that updates database (INSERT, DELETE, etc).
     * @param $query
     * @param array $params bindable parameter array
     * @return int Number of rows affected in the database, 0 on error.
     */
    public function execUpdateQuery($query, $params) {
        if (!$query || !$this->connected) return 0;

        $num_rows = 0;
        $stmt = $this->conn->prepare($query);
        if ($stmt) {
            if ($this->bindParams($stmt, $params)) {
                $ret = $stmt->execute();
                if ($ret) {
                    $num_rows = $stmt->affected_rows;
                } else {
                    $this->last_error = $stmt->error;
                }
            }
        } else {
            $this->last_error = $this->conn->error;
        }
        return $num_rows;
    }

    /**
     * Execute a select query
     * @param $query
     * @param array $params bindable parameter array, can be empty or false
     * @return \mysqli_result|bool Result set or false on error
     */
    public function execSelectQuery($query, $params) {
        if (!$query || !$this->connected) return false;

        $result = false;
        $stmt = $this->conn->prepare($query);
        if ($stmt) {
            if ($this->bindParams($stmt, $params)) {
                $result = $stmt->execute();
                if ($result) {
                    $result = $stmt->get_result();
                    if (!$result) {
                        $this->last_error = $stmt->error;
                    }
                } else {
                    $this->last_error = $stmt->error;
                }
            } else {
                $this->last_error = "Error while binding params for query $query";
            }
        } else {
            $this->last_error = $this->conn->error;
        }

        return $result;
    }

  /**
   * Run a select query which is supposed to return a single row
   * @param string $query SQL query, ? are allowed for param values
   * @param array $params parameters to be used instead of ? in the query
   * @return array|null Array with associative names or null on error
   */
  public function execSingleRowQuery($query, $params) {
    $results = $this->execSelectQuery($query, $params);
    if ($results) {
      return mysqli_fetch_assoc($results);
    } else {
      return null;
    }
  }
    /**
     * Bind parameter values to prepared statement
     * @param \mysqli_stmt $stmt
     * @param array $params
     * @return bool true on success, false on error. Updates $this->>last_error in case of failure.
     */
    private function bindParams($stmt, $params) {
        $types = self::getParamTypeString($params);
        if ($types) {
            // Append the type string to the beginning of param array - it must be the first argument to bind_param function
            // And add references to all parameters
            $param_refs = [$types];
            foreach ($params as $key => $val) {
                $param_refs[] = &$params[$key];
            }
            // Call $stmt->bind_param($types, $params[0], $params[1], ...)
            $ret = call_user_func_array([&$stmt, "bind_param"], $param_refs);
            if (!$ret) {
                $this->last_error = "Could not bind params to query";
                return false;
            }
        }
        return true;
    }

    /**
     * Return a string specifying one character for each parameter. This string can be passed into
     * Statement::prepare function
     * @param array $params Variable number of parameters
     * @return string
     */
    private static function getParamTypeString($params) {
        if (!$params) return "";
        $s = "";
        foreach ($params as $p) {
            if (is_int($p)) {
                $s .= "i";
            } else if (is_double($p)) {
                $s .= "d";
            } else if (is_string($p)) {
                $s .= "s";
            } else {
                $s .= "b"; // BLOB
            }
        }
        return $s;
    }

  /**
   * @return bool True when duplicate error happened during the last query (INSERT or UPDATE)
   */
  public function wasDuplicate() {
    return strpos($this->last_error, "Duplicate entry") !== false;
  }
}