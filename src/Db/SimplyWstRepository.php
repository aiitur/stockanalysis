<?php

namespace Db;

use Model\Industry;
use Model\SimplyWstStockData;

/**
 * DB handler for SimplyWall.St data
 */
class SimplyWstRepository {
  private function __construct() {
  }

  private static $instance;

  /**
   * Return a singleton.
   * @return SimplyWstRepository
   */
  public static function getInstance() {
    if (!self::$instance) {
      self::$instance = new SimplyWstRepository();
    }
    return self::$instance;
  }

  /**
   * Insert a row into SimplyWst extended stock info table
   * @param SimplyWstStockData $stock
   * @return int 1 when insert was made successfully, 0, when duplicate row already existed in the table, -1 on error
   */
  public function add(SimplyWstStockData $stock) {
    $db = DbConn::getInstance();
    if (!$db) return -1;

    $query = "INSERT INTO `simplywst_stocks` (simplywst_ticker, company_id, description, industry, industry_id, 
                                info_url, raw_json, trading_item_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
    if ($stock->industry) {
      $industry_id = $stock->industry->id;
      $industry_name = $stock->industry->name;
    } else {
      $industry_id = $industry_name = null;
    }
    $params = [$stock->simplywst_ticker, $stock->company_id, $stock->description, $industry_name, $industry_id,
      $stock->info_url, $stock->raw_json, $stock->trading_item_id];
    $row_count = $db->execUpdateQuery($query, $params);
    if ($row_count == 0 && !$db->wasDuplicate()) {
      // Duplicate entry is ok, all other errors are not
      echo "ERROR: " . DbConn::getLastError() . "\n";
      $row_count = -1;
    }
    return $row_count;
  }

  /**
   * Get a single record for a stock with given exchange symbol, such as OB:KID
   * @param $symbol
   * @return SimplyWstStockData|null
   */
  public function get($symbol) {
    $db = DbConn::getInstance();
    if (!$db) return null;

    $query = "SELECT s.ticker, s.name, s.exchange_id, s.yahoo_ticker, ss.* FROM `simplywst_stocks` ss  
        INNER JOIN stocks s ON ss.simplywst_ticker = s.simplywst_ticker WHERE s.exchange_symbol = ?";
    $params = [$symbol];
    $stock = null;
    $row = $db->execSingleRowQuery($query, $params);
    if ($row) {
      if ($row["industry_id"] && $row["industry"]) {
        $industry = new Industry($row["industry_id"], $row["industry"]);
      } else {
        $industry = null;
      }
      $stock = new SimplyWstStockData($row["ticker"], $symbol, $row["name"], $row["exchange_id"], $row["info_url"],
        $row["company_id"], $row["trading_item_id"], $row["description"], $industry);
      $stock->yahoo_ticker = $row["yahoo_ticker"];
      $stock->raw_json = $row["raw_json"];
      $stock->simplywst_ticker = $row["simplywst_ticker"];
    }
    return $stock;
  }

  /**
   * Insert raw data from SimplyWall.St web about a single stock into the DB
   * @param string $simplywst_ticker OB:TEL, etc
   * @param string $raw_data Raw data received from the API
   * @return bool True on success, false otherwise
   */
  public function saveRawData(string $simplywst_ticker, string &$raw_data) {
    $db = DbConn::getInstance();
    if (!$db) return false;

    $query = "INSERT INTO `simplywst_raw_data` (simplywst_ticker, raw_data) VALUES (?, ?)";
    $params = [$simplywst_ticker, $raw_data];
    $row_count = $db->execUpdateQuery($query, $params);
    if ($row_count != 1) {
      echo "SQL ERROR: " . DbConn::getLastError() . "\n";
    }
    return $row_count == 1;
  }

  public function getLatestRawData(string $simplywst_ticker) {
    $db = DbConn::getInstance();
    if (!$db) return null;

    $query = "SELECT raw_data FROM `simplywst_raw_data` s WHERE s.simplywst_ticker = ? ORDER BY added_to_db DESC LIMIT 1";
    $params = [$simplywst_ticker];
    $raw_data = null;
    $row = $db->execSingleRowQuery($query, $params);
    if ($row) {
      $raw_data = json_decode($row["raw_data"]);
    }
    return $raw_data;
  }

  /**
   * Get most recent RAW SimplyWall.St data stored for a stock
   * @param string $simplywst_ticker
   * @return object|null JSON object received from the API, decoded as PHP object
   */
  public function getRawData(string $simplywst_ticker) {
    $db = DbConn::getInstance();
    if (!$db) return null;

    $query = "SELECT raw_data FROM simplywst_raw_data WHERE simplywst_ticker = ? ORDER BY added_to_db DESC LIMIT 1";
    $params = [$simplywst_ticker];
    $row = $db->execSingleRowQuery($query, $params);
    if ($row) {
      return json_decode($row["raw_data"]);
    } else {
      return null;
    }
  }
}