<?php

namespace Db;

use Model\MagicFormulaStockData;
use Utils\RankedStockList;

/**
 * Handles MagicFormula Top50 table storage in SQL database
 */
class MagicFormulaRepository {
  const TABLE_NAME = "magic_formula_top50";

  /**
   * Save entries from a table to DB
   * @param RankedStockList $table
   * @return bool True on success, false on error.
   */
  public function save(RankedStockList $table) {
    $db = DbConn::getInstance();
    if (!$db) return false;

    $stockRepo = StockRepository::getInstance();
    $stocks = $stockRepo->getAll();

    $entries = $table->getEntries();
    // Count how many items added to DB
    $entries_added = 0;
    $stocks_added = 0;

    /**
     * @var int $rating
     * @var MagicFormulaStockData $topStock
     */
    foreach ($entries as $rating => $topStock) {
      $symbol = $topStock->exchange_symbol;
      if (!isset($stocks[$symbol])) {
        // This stock is not found in the DB yet, store it
        $a = $stockRepo->add($topStock);
        if ($a < 0) break;
        $stocks_added += $a;
      }
      $date = $table->getDate();

      $a = $this->add($symbol, $date, $rating, $topStock->market_cap, $topStock->price_date, $topStock->quarter_date);
      if ($a < 0) break;
      $entries_added += $a;
    }

    echo "$stocks_added Stocks added to DB\n";
    echo "$entries_added Top50 entries added to DB\n";

    return true;
  }

  /**
   * Save a top-rated stock to SQL table
   * @param string $exchange_symbol Symbol of the stock (unique, including the exchange code): OB:EL, NasdaqGS:TSLA, etc.
   * @param \DateTime $date Date when this rating was checked
   * @param int $rating InvestTech rating of this stock at the given moment
   * @param float $market_cap Market capitalization, in million USD
   * @param string $price_date Date of the price used in this calculation
   * @param string $quarter_date Date of the quarter financials used in the calculations
   * @return int 1 when insert was made successfully, 0, when duplicate row already existed in the table, -1 on error
   */
  private function add(string $exchange_symbol, \DateTime $date, int $rating, float $market_cap, string $price_date, string $quarter_date) {
    $db = DbConn::getInstance();
    if (!$db) return -1;

    $table_name = self::TABLE_NAME;

    $query = "INSERT INTO $table_name (ticker, insert_time, rating, market_cap, price_date, quarter_date) VALUES (?, FROM_UNIXTIME(?), ?, ?, ?, ?)";
    $params = [$exchange_symbol, $date->getTimestamp(), $rating, $market_cap, $price_date, $quarter_date];
    $row_count = $db->execUpdateQuery($query, $params);
    if ($row_count == 0 && !$db->wasDuplicate()) {
      // Duplicate entry is ok, all other errors are not
      echo "ERROR: " . DbConn::getLastError() . "\n";
      $row_count = -1;
    }
    return $row_count;

  }
}