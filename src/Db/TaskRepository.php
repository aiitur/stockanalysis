<?php

namespace Db;

use Tasks\Task;

/**
 * Handles task storage in DB
 */
class TaskRepository {
  // Fields used in task selection
  const FIELDS = "*, UNIX_TIMESTAMP(time_created) AS tc, UNIX_TIMESTAMP(time_started) AS ts,  
       UNIX_TIMESTAMP(time_finished) AS tf";

  /**
   * Private constructor for singleton needs.
   */
  private function __construct() {
  }

  private static $instance;

  /**
   * Return a singleton.
   * @return TaskRepository
   */
  public static function getInstance() {
    if (!self::$instance) {
      self::$instance = new TaskRepository();
    }
    return self::$instance;
  }

  /**
   * Get the first task with given state
   * @param int $state
   * @return Task|null
   */
  private static function getNextTaskWithState(int $state) {
    $db = DbConn::getInstance();
    if (!$db) return null;
    $fields = self::FIELDS;
    $query = "SELECT $fields FROM `task` WHERE state = ? LIMIT 1";
    $params = [$state];
    $row = $db->execSingleRowQuery($query, $params);
    return self::createTaskFromDbRow($row);
  }

  /**
   * Select the next task currently waiting in the queue to be executed.
   * @return Task|null
   */
  public function getNextPendingTask() {
    return self::getNextTaskWithState(Task::ST_NEW);
  }

  /**
   * Get the first task which is still in progress
   * @return Task|null
   */
  public function getTaskInProgress() {
    return self::getNextTaskWithState(Task::ST_RUNNING);
  }

  /**
   * Update a task in the DB
   * @param Task $task
   * @return bool True on success, false otherwise
   */
  public function update(Task $task) {
    $db = DbConn::getInstance();
    if (!$db) return null;
    $query = "UPDATE `task` SET pid = ?, state = ?, error = ?, time_started = FROM_UNIXTIME(?), 
                  time_finished = FROM_UNIXTIME(?) WHERE id = ?";
    $params = [$task->getPid(), $task->getState(), $task->error, $task->getStartTimestamp(),
      $task->getFinishTimestamp(), $task->id];
    $row_count = $db->execUpdateQuery($query, $params);
    return $row_count == 1;
  }

  /**
   * Create a task from a row returned from DB query
   * @param array $row
   * @return Task|null
   */
  private static function createTaskFromDbRow($row) {
    if ($row) {
      $task = new Task($row["id"], $row["class"], $row["pid"], $row["state"], $row["error"]);
      $task->initCreationTime($row["tc"]);
      $task->initStartTime($row["ts"]);
      $task->initFinishTime($row["tf"]);
      if ($row["max_time"]) {
        $task->max_allowed_time = (int)$row["max_time"];
      }
      if ($row["arguments"]) {
        $arguments = json_decode($row["arguments"]);
        if ($arguments) {
          $task->arguments = $arguments;
        }
      }
      return $task;
    } else {
      return null;
    }
  }

  /**
   * Schedule a task, add an entry for it in the database
   * @param string $runner_class Runner that will execute the task
   * @param int $max_time Maximum time (in seconds) that the task is allowed to run. Use zero or null to have unlimited time.
   * @param mixed $arguments Optional arguments to pass to the script
   * @return bool True of success, false otherwise
   */
  public function add(string $runner_class, $max_time, $arguments) {
    $db = DbConn::getInstance();
    if (!$db) return false;

    if ($max_time) {
      $max_time = null;
    }
    // Arguments must be encoded as JSON
    if ($arguments) {
      $arguments = json_encode($arguments);
    } else {
      $arguments = null;
    }

    $query = "INSERT INTO `task` (class, max_time, arguments) VALUES (?, ?, ?)";
    $params = [$runner_class, $max_time, $arguments];
    $row_count = $db->execUpdateQuery($query, $params);
    return $row_count == 1;
  }

}