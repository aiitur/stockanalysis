<?php

if (stripos(php_uname(), "Linux") !== false) {
  // This is a production machine running Linux (Docker)
  require_once("platform_config_prod.php");
} else {
  // This must be a dev machine
  require_once("platform_config_dev.php");
}

require_once("autoloader.php");
