<?php

// Platform-specific configuration: platform.config.php
// This file should NOT be in the GIT. Instead, an example platform-config-template.php should be in the git
// Devs should take the template, fill in the local parameters
// Whenever someone updates any config parameters, the template file must be updated as well!

/// SQL database config
define("DEBUG", false); // Set this to true to enable some more debug output in dev environment
define("DEV", false); // Set this to true to specify that this is development environment
define("DB_HOST", "localhost");
define("DB_USER", "fintech");
define("DB_PWD", "your-pwd-here");
define("DB_NAME", "fintech");
define("DB_PORT", "3306");

// InvestTech config
define("INVESTTECH_EMAIL", "user@gmail.com");
define("INVESTTECH_PASSWORD", "your-pwd-here");

// Magicformulainvesting.com config
define("MAGIC_FORMULA_EMAIL", "john@doe.com");
define("MAGIC_FORMULA_PASSWORD", "your-pwd-here");

// Timezone config.
date_default_timezone_set('Europe/Oslo');