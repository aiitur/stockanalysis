<?php

namespace Services;

use Model\InvestTechStockData;
use Model\StockData;
use Utils\RankedStockList;

class InvestTechService {

  /**
   * Compare two lists of stocks - stocks that were in the InvestTech sell list (list 1), and stock in buy list (list 2)
   * Report a list of intervals for each stock: when was it in each list.
   * @param array $list1 array with dates as indices, RankedStockList as value
   * @param array $list2 array with dates as indices, RankedStockList as value
   * @return array in the format:
   *   [
   *     ticker => [
   *       "stock" => stock_name,
   *       "intervals" => [ // Intervals arranged by start date
   *         "dates" => [date1, dte2, ... ] days when the stock was in the list
   *         "list" => 1 or 2 // Whether the stock was in the 1st or second list
   *       ],
   *     ...
   *   ]
   */
  public static function getListIntervals(array $list1, array $list2) {
    $dates = array_merge(array_keys($list1), array_keys($list2));
    $dates = array_unique($dates);
    sort($dates); // Make sure dates are in ascending order
    $intervals = [];
    $prev_date = "";
    foreach ($dates as $date) {
      if (isset($list1[$date])) {
        self::appendTop50TableToIntervals($intervals, $date, $prev_date, $list1[$date], 1);
      }
      if (isset($list2[$date])) {
        self::appendTop50TableToIntervals($intervals, $date, $prev_date, $list2[$date], 2);
      }
      $prev_date = $date;
    }
    return $intervals;
  }

  /**
   * Takes ranked stock list for one date, appends it to existing intervals
   * @param array $intervals Intervals as returned by getListIntervals
   * @param string $date Date when this top50 table was retrieved
   * @param string $prev_date Previous date that was processed
   * @param RankedStockList $list
   * @param int $list_id 1 or 2 - which initial list does this top50 table belong to
   */
  private static function appendTop50TableToIntervals(array &$intervals, string $date, string $prev_date, RankedStockList $list, int $list_id) {
    $l = $list->getEntries();
    /**
     * @var int $rank
     * @var StockData $s
     */
    foreach ($l as $rank => $s) {
      if (!isset($intervals[$s->exchange_symbol])) {
        // First time this stock appears in the list
        $intervals[$s->exchange_symbol] = [
          "stock" => $s->name,
          "intervals" => []
        ];
      }

      $si = &$intervals[$s->exchange_symbol]["intervals"];

      // Extend an existing interval if the stock is in the same list still, start a new interval otherwise
      if (count($si) > 0) {
        $last_index = count($si) - 1;
        $last_interval = &$si[$last_index];
        // Check if there have been dates reported without the stock in any table, register that as a break, start a new interval
        if ($last_interval["list"] == $list_id && (!$prev_date || $prev_date == end($last_interval["dates"]))) {
          $same_list = true;
        } else {
          $same_list = false;
        }
      } else {
        $same_list = false;
      }

      if ($same_list) {
        $last_interval["dates"][] = $date;
      } else {
        $si[] = [
          "dates" => [$date],
          "list" => $list_id
        ];
      }
    }
  }
}