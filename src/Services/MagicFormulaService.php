<?php

namespace Services;

use Model\MagicFormulaStockData;
use Utils\RankedStockList;

/**
 * Functions for magic formula related things, not dependent on API or Database
 */
class MagicFormulaService {

  /**
   * Merge two tables: top 30 and top 50 stocks.
   * @param RankedStockList $top30_table
   * @param RankedStockList $top50_table
   * @return RankedStockList A new table containing top 30 stocks in alphabetical order first, then remaining top 50 stocks
   * in alphabetical order
   */
  public static function mergeTables(RankedStockList $top30_table, RankedStockList $top50_table) {
    $merged_table = clone $top30_table;
    $top30_stocks = [];
    /** @var MagicFormulaStockData $stock */
    foreach ($merged_table->getEntries() as $stock) {
      $top30_stocks[$stock->ticker] = $stock;
    }
    $nr = 31;
    /** @var MagicFormulaStockData $stock */
    foreach ($top50_table->getEntries() as $stock) {
      if (!isset($top30_stocks[$stock->ticker])) {
        $merged_table->add($nr++, $stock);
      }
    }
    return $merged_table;
  }
}