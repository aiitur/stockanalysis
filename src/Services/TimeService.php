<?php

namespace Services;

use DateTime;
use DateTimeZone;
use DateInterval;
use Model\TimedEntity;
use Model\TimePeriod;


/**
 * Class TimeService
 * Handles time calculations
 */
class TimeService {
  const SECONDS_IN_MIN = 60;
  const SECONDS_IN_HOUR = 60 * self::SECONDS_IN_MIN;
  const SECONDS_IN_DAY = 24 * self::SECONDS_IN_HOUR;
  const DAYS_IN_YEAR = 365;
  const SECONDS_IN_YEAR = self::SECONDS_IN_DAY * self::DAYS_IN_YEAR;

  /**
   * Round a timestamp to a given resolution: 1min, 15min, etc
   * @param DateTime|string|int $time timestamp
   * @param int $resolution aggregation resolution, in minutes. For example, for 1h rounding,
   * use 60 as resolution
   * @param bool $roundDown if true, round down, if false, round up
   * @return DateTime|null rounded timestamp, or null on error
   */
  public static function roundTime($time, $resolution, $roundDown = true) {
    if (!$time || !is_int($resolution) || !$resolution) {
      return null;
    }
    if (is_string($time)) {
      $time = self::stringToDateTime($time);
    }
    if (!is_int($time)) {
      $timestamp = $time->getTimestamp();
    } else {
      // If int is given, we suppose it is a timestamp
      $timestamp = $time;
    }
    $modulo = $resolution * 60; // Get modulo in seconds

    try {
      // Hack for rounding to weeks!
      // Zero day was Thursday, therefore we add 3 days
      if ($resolution == 60 * 24 * 7) {
        // Get last monday. the "Last monday" conversion works with
        $tzoffset = (new DateTime())->getOffset();
        // Last monday returns "previous monday" if today is Monday, therefore we need to have +1 day
        $localTomorrow = $timestamp + $tzoffset + (60 * 60 * 24);
        $lmTimestamp = strtotime("Last monday UTC", $localTomorrow);
        $mondayStr = gmdate("Y-m-d", $lmTimestamp);
        $roundedTimestamp = self::stringToDateTime($mondayStr)->getTimestamp();
      } else {
        $roundedTimestamp = $timestamp - ($timestamp % $modulo);
      }

      if (!$roundDown) {
        $roundedTimestamp += $modulo; // round up
      }

      $d = new DateTime();
      $d->setTimezone(self::getUtcTimezone());
      $d->setTimestamp($roundedTimestamp);
      return $d;
    } catch (\Exception $e) {
      return null;
    }
  }

  private static $UTC;

  /**
   * Return a timezone representing the UTC (+00:00)
   * @return DateTimeZone
   */
  public static function getUtcTimezone() {
    if (!self::$UTC) {
      self::$UTC = new DateTimeZone("UTC");
    }
    return self::$UTC;
  }

  /**
   * Takes a timestamp in string format, return DateTime, with UTC timezone (+00:00)!
   * @param $timeString
   * @return DateTime|null
   */
  public static function stringToDateTime($timeString) {
    try {
      $d = new DateTime($timeString);
      $offset = $d->getOffset();
      // Convert to UTC timezone, strip the offset
      $d->setTimezone(self::getUtcTimezone());
      $d->setTimestamp($d->getTimestamp() + $offset);
      return $d;
    } catch (\Exception $e) {
      return null;
    }
  }

  /**
   * Takes a timestamp as an integer, return DateTime, with UTC timezone (+00:00)!
   * @param int $timestamp
   * @return DateTime|null
   */
  public static function timestampToDateTime($timestamp) {
    try {
      $d = new DateTime();
      // Convert to UTC timezone, strip the offset
      $d->setTimezone(self::getUtcTimezone());
      $d->setTimestamp($timestamp);
      return $d;
    } catch (\Exception $e) {
      return null;
    }
  }


  /**
   * Increase given timestamp by $n minutes. Warning - the $time object is modified!
   * @param DateTime $time
   * @param int $n
   * @return DateTime|null new timestamp or null on error
   */
  public static function addMinutes(DateTime $time, $n) {
    if (!is_int($n)) return null;
    $n = (int)$n;
    try {
      $t = new DateTime('now', self::getUtcTimezone());
      $t->setTimestamp($time->getTimestamp());
      if ($n >= 0) {
        $t->add(new DateInterval("PT" . $n . "M"));
      } else {
        // Negative amount of minutes
        $n *= -1;
        $t->sub(new DateInterval("PT" . $n . "M"));
      }
      return $t;
    } catch (\Exception $ex) {
      return null;
    }
  }

  /**
   * Take a timestamp in one of several formats, return a DateTime object
   * @param DateTime|string|int $time
   * @return DateTime|null timestamp or null on error
   */
  public static function anyToTimestamp($time) {
    if (is_string($time)) {
      // String timestamp
      return TimeService::stringToDateTime($time);
    } else if (is_numeric($time)) {
      // Int timestamp
      return TimeService::timestampToDateTime($time);
    } else if (is_a($time, "DateTime")) {
      return $time;
    } else {
      return null;
    }
  }

  /**
   * Calculate difference between two timestamps, in seconds
   * @param DateTime $time1
   * @param DateTime $time2
   * @return int difference in seconds: time2 minus time1; or -1 on error
   */
  public static function timeDiff($time1, $time2) {
    if (!$time1 || !$time2) return -1;
    return $time2->getTimestamp() - $time1->getTimestamp();
  }

  /**
   * Check two timestamps, return true if one is exactly one minute after the other
   * @param DateTime $time1
   * @param DateTime $time2
   * @return boolean
   */
  public static function isNextMinute($time1, $time2) {
    return self::timeDiff($time1, $time2) == 60;
  }

  /**
   * Go through an array of timed entities, return the total period covering all of them
   * @param array $timedEntities
   * @return TimePeriod containing all the entities, or null on error
   */
  public static function getPeriod(& $timedEntities) {
    if (!$timedEntities) return null;

    $pp = new TimePeriod();
    /** @var TimedEntity $te */
    foreach ($timedEntities as $te) {
      $time = $te->getTime();
      if ($time) {
        $pp->extend($time->getTimestamp());
      }
    }
    return $pp;
  }

  /**
   * Compare all consecutive timestamps, and return the longest uninterrupted interval
   * @param array $timestamps array containing timestamps. Can be strings, can be unix timestamps
   * @param int $allowedDifference allowed difference between two consecutive timestamps, in seconds
   * @return null|TimePeriod contains the first and last timestamp of the interval. The smallest timestamp
   * will always be the first
   */
  public static function getUninterrupted(& $timestamps, $allowedDifference) {
    if (empty($timestamps)) {
      return null;
    }
    if (count($timestamps) == 1) {
      $dt0 = self::anyToTimestamp($timestamps[0])->getTimestamp();
      return new TimePeriod($dt0, $dt0);
    }

    // Detect whether the timestamps are increasing or decreasing
    $direction = null;
    $ts0 = self::anyToTimestamp($timestamps[0]);
    $ts1 = self::anyToTimestamp($timestamps[1]);
    $sign = $ts0->getTimestamp() < $ts1->getTimestamp() ? 1 : -1;

    // First timestamp in the interval
    $tfirst = $ts0->getTimestamp();
    // Previous timestamp to be compared with current
    $tprev = $tfirst;

    for ($i = 1; $i < count($timestamps); ++$i) {
      $dt = self::anyToTimestamp($timestamps[$i]);
      $tcurr = $dt->getTimestamp();
      $diff = ($tcurr - $tprev) * $sign;
      if ($diff < 0 || $diff > $allowedDifference) {
        // First wrong timestamp
        break;
      }
      $tprev = $tcurr;
    }
    // When timestamps are descending, we reverse the order to ensure that smaller timestamp is always the first
    return $sign > 0 ? new TimePeriod($tfirst, $tprev) : new TimePeriod($tprev, $tfirst);
  }

  /**
   * Format timestamp as a string that includes the timezone
   * @param DateTime $time
   * @return string Formatted string.
   */
  public static function format($time) {
    return $time ? $time->format("Y-m-d H:i:sO") : null;
  }

  /**
   * Get a timestamp representing this moment, with UTC timezone
   * @return DateTime|null
   */
  public static function now() {
    try {
      return new DateTime("now", self::getUtcTimezone());
    } catch (\Exception $e) {
      return null;
    }
  }

  /**
   * Format duration between two timestamps nicely: 15s, 5m, 1h, etc
   * @param int $duration_sec Duration in seconds
   * @return string
   */
  public static function getNiceDuration($duration_sec) {
    if ($duration_sec >= self::SECONDS_IN_DAY) {
      $days = $duration_sec / self::SECONDS_IN_DAY;
      $full_days = (int)$days;
      $remainder = $duration_sec - $full_days * self::SECONDS_IN_DAY;
      if ($remainder >= self::SECONDS_IN_HOUR) {
        $hours = round($remainder / self::SECONDS_IN_HOUR);
        $result = "{$full_days}d {$hours}h";
      } else {
        $result = "{$full_days}d";
      }
    } else if ($duration_sec >= self::SECONDS_IN_HOUR) {
      $hours = $duration_sec / self::SECONDS_IN_HOUR;
      $full_hours = (int)$hours;
      $remainder = $duration_sec - $full_hours * self::SECONDS_IN_HOUR;
      if ($remainder >= self::SECONDS_IN_MIN) {
        $minutes = (int)($remainder / self::SECONDS_IN_MIN);
        $result = "{$full_hours}h {$minutes}m";
      } else {
        $result = "{$full_hours}h";
      }
    } else if ($duration_sec >= self::SECONDS_IN_MIN) {
      $minutes = (int)($duration_sec / self::SECONDS_IN_MIN);
      $result = "{$minutes}m";
    } else {
      $result = "{$duration_sec}s";
    }
    return $result;
  }
}