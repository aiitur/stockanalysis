<?php

namespace Services;

/**
 * Handles sending of HTTP GET and POST requests
 * Class HttpRequester
 */
class HttpRequester {
  private $cookie_file_path;
  private $ch;
  private $random_sleep;

  // Pretend to be a Chrome browser
  const AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36";

  /**
   * HttpRequester constructor.
   * @param string $cookie_file_path Path to file where cookies for CURL will be stored.
   * @param bool $random_sleep When true, random sleep will be done before each request to simulate human behaviour
   */
  public function __construct($cookie_file_path, $random_sleep = true) {
    $this->cookie_file_path = $cookie_file_path;
    $this->ch = curl_init();

    $this->setCertificatePath(getcwd() . "/ssl-certs/cacert.pem");

    // Set up SSL certificate verification
    curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, true);
    curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 2);

    // Read previously stored cookies
    if ($this->cookie_file_path) {
      curl_setopt($this->ch, CURLOPT_COOKIEFILE, $this->cookie_file_path);
    }
    $this->random_sleep = $random_sleep;
  }

  /**
   * Set path to certificate file
   * @param string $cert_fpath Full path to certificate key file
   */
  public function setCertificatePath(string $cert_fpath) {
    curl_setopt($this->ch, CURLOPT_CAINFO, $cert_fpath);
  }

  /**
   * Send an HTTP GET request
   * @param string $url URL to the page, including the file path and GET parameters
   * @param string $referer The "previous" page which led to this one
   * @return false|string The HTML body of the response or false on error
   */
  function sendGet($url, $referer) {
    return $this->sendHttpRequest($url, $referer, false, []);
  }

  /**
   * Send an HTTP POST request
   * @param string $url URL to the page, including the file path and GET parameters
   * @param string $referer The "previous" page which led to this one
   * @param array|string $post_data array containing HTTP POST data
   * @param bool $is_json When true, the post data will be sent as application/json
   * @return false|string The HTML body of the response or false on error
   */
  function sendPost($url, $referer, $post_data, bool $is_json = false) {
    return $this->sendHttpRequest($url, $referer, true, $post_data, $is_json);
  }

  /**
   * Closes CURL object, stores obtained cookies in the file.
   */
  function closeSession() {
    echo "Closing CURL session.\n";
    if ($this->cookie_file_path) {
      // Write cookies to file for the next session
      curl_setopt($this->ch, CURLOPT_COOKIEJAR, $this->cookie_file_path);
    }
    curl_close($this->ch);
  }

  /**
   * Send and HTTP GET or POST request
   * @param string $url URL to the page, including the file path and GET parameters
   * @param string $referer The "previous" page which led to this one
   * @param $is_post bool When true, send HTTP POST, when false, sed HTTP GET
   * @param $post_data array|string Array of POST data parameters or a single string. Ignored for GET.
   * @param bool $is_json When true, the post data will be sent as application/json
   * @return false|string Response body or false on error
   */
  public function sendHttpRequest($url, $referer, $is_post, $post_data, $is_json = false) {
    curl_setopt($this->ch, CURLOPT_NOBODY, false);
    curl_setopt($this->ch, CURLOPT_URL, $url);
    curl_setopt($this->ch, CURLOPT_USERAGENT, self::AGENT);
    curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($this->ch, CURLOPT_REFERER, $referer);
    curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 0);
    if ($is_post) {
      curl_setopt($this->ch, CURLOPT_HTTPGET, false);
      curl_setopt($this->ch, CURLOPT_POST, true);
      if (is_array($post_data)) {
        $post_body = $this->mergePostData($post_data);
      } else {
        $post_body = $post_data;
        if ($is_json) {
          curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
              'Content-Type: application/json',
              'Content-Length: ' . strlen($post_data))
          );
        }
      }
      curl_setopt($this->ch, CURLOPT_POSTFIELDS, $post_body);
    } else {
      curl_setopt($this->ch, CURLOPT_POST, false);
      curl_setopt($this->ch, CURLOPT_HTTPGET, true);
    }
    if ($this->random_sleep && !DEV) {
      // Make this look like a human-generate request - sleep some random time
      $this->sleepRandomTime(5, 10);
    }
    return curl_exec($this->ch);
  }

  /**
   * Sleep a random time between min_time and max_time seconds (inclusive).
   * @param $min_time
   * @param $max_time
   */
  private function sleepRandomTime($min_time, $max_time) {
    $time_to_sleep = rand($min_time, $max_time);
    echo " (sleeping $time_to_sleep...) \n";
    sleep($time_to_sleep);
  }

  /**
   * Merge an array with key: value pairs into a string that can be posted as HTTP POST body
   * @param array $post_data
   * @return string key1=val1&key2=val2...
   */
  private function mergePostData(array $post_data) {
    $temp = [];
    foreach ($post_data as $key => $val) {
      $temp[] = "{$key}={$val}";
    }
    return implode("&", $temp);
  }

  /**
   * Return error message from last CURL request
   * @return string
   */
  public function getLastError() {
    return curl_error($this->ch);
  }

  /**
   * Return true if the last HTTP response was a redirect
   * @return bool
   */
  public function wasRedirected() {
    $response_code = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
    return $response_code == 302;
  }

  /**
   * Return the last redirect target URL
   * @return mixed
   */
  public function getRedirectUrl() {
    return curl_getinfo($this->ch, CURLINFO_REDIRECT_URL);
  }

}
