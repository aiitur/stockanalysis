<?php

namespace Services;

/**
 * Handles number calculations. Does not know anything about SQL. Should be treated as a Singleton.
 */
class NumberService {
  const BIN_SIZES = [0.01, 0.02, 0.05, 0.1, 0.2, 0.5, 1.0, 2.0, 5.0, 10.0, 20.0, 50.0, 100.0, 200.0, 500.0, 1000.0];
  const BOUNDARY_ROUNDING_DELTA = 0.02; // How much to round up/down numbers close to a "round number" boundary
  const BOUNDARY_MAX_DISTANCE = 0.09; // How gar from a round boundary a number can be to be considered "close to boundary"

  /**
   * Round  a given number to specific number of significant digits
   * https://en.wikipedia.org/wiki/Significant_figures
   *
   * Source code adapted from http://php.net/manual/en/function.round.php
   *
   * @param string $number decimal number, in string representation
   * @param int $sigDigits number of significant digits to use
   * @return string rounded number or null on error
   */
  public static function round($number, $sigDigits) {
    if (!$sigDigits || $sigDigits < 0) {
      return null;
    }
    if (is_float($number)) {
      $number = "$number"; // Make sure it's string
    }
    if (!is_string($number)) {
      return null;
    }
    if ($sigDigits === 0) {
      return 0; // Rounding to zero significant digits means rounding out all information
    }

    $dp = (int)floor($sigDigits - log10(abs($number)));
    // Round as a regular number.
    $number = round($number, $dp);
    // Leave the formatting to format_number(), but always format 0 to 0dp.
    return number_format($number, 0 == $number ? 0 : $dp, ".", "");
  }

  /**
   * Calculate difference between two decimals: $a and $b, and return it as a percentage relative to the largest number
   * @param string $a decimal
   * @param string $b decimal
   * @param int $round_digits how many significant digits to keep in the result
   * @param bool $relative_to_big When true, calculate percentage relative to the biggest numbers, when false: relative to the smallest
   * @return string difference in percent, decimal. Result may not make sense for negative numbers!
   */
  public static function diffPercent($a, $b, $round_digits = 3, $relative_to_big = true) {
    if (bccomp($a, $b) == 1) {
      $big = $a;
      $small = $b;
    } else {
      $big = $b;
      $small = $a;
    }
    $reference = $relative_to_big ? $big : $small;

    // percent = (big - small) / big * 100
    return self::round(bcmul(bcdiv(bcsub($big, $small), $reference), "100"), $round_digits);
  }

  /**
   * Get min of two decimals, represented as strings.
   * @param string $a
   * @param string $b
   * @return string|null The min number. IF one of them is null, return the non-null value! If both are null, return null.
   */
  public static function min($a, $b) {
    if (bccomp($a, $b) == -1) {
      if ($a !== null) {
        return $a;
      } else {
        return $b;
      }
    } else {
      if ($b !== null) {
        return $b;
      } else {
        return $a;
      }
    }
  }

  /**
   * Get max of two decimals, represented as strings
   * @param string $a
   * @param string $b
   * @return string
   */
  public static function max($a, $b) {
    if (bccomp($a, $b) == 1) {
      return $a;
    } else {
      return $b;
    }
  }

  /**
   * Return $p percent subtracted from $num
   * @param string $num Decimal string
   * @param string $p Decimal string, represents percent
   * @return string Decimal string
   */
  public static function subPercent($num, $p) {
    if ($num == null) {
      return null;
    }
    $p_dec = bcdiv($p, "100"); // go from percent to decimal
    $remain = bcsub("1.0", $p_dec); // 1 - decimal
    return bcmul($num, $remain);
  }

  /**
   * Return $p percent added to  $num
   * @param string $num Decimal string
   * @param string $p Decimal string, represents percent
   * @return string Decimal string
   */
  public static function addPercent($num, $p) {
    return self::subPercent($num, bcmul($p, "-1")); // subtract negative percent
  }

  /**
   * Convert precision such as 1, 2, 4 to decimal precision: 0.1, 0.01, 0.0001 to be used for roundToDecimal
   * @param int $precision Precision as a number of decimal digits
   * @return string decimal precision
   */
  public static function precToDecPrec($precision) {
    if ($precision < 1) return 0;
    $d = 1.0;
    for ($i = 1; $i <= $precision; ++$i) {
      $d = bcdiv($d, "10");
    }
    return $d;
  }

  /**
   * Round a number to given desired decimal precision
   * @param string $num Decimal string
   * @param string $precision Decimal string.  Must be a decimal, with 0..n zeros and a single 1.
   * For example, 10.0 means that num will be rounded to tens. 0.01 means that num will be rounded to 2 decimal digits.
   * @return string|bool Rounded decimal string or false on error
   */
  public static function roundToDecimal($num, $precision) {
    $size = bccomp($precision, "1.0");
    if ($size >= 0) {
      // dec >= 1, round to integer
      $num = (int)$num;
      $precision = (int)$precision;

      // divide dec and number by 10 in each step, add one zero to multiplier
      $multiplier = 1;
      while ($precision > 1) {
        $multiplier *= 10;
        $precision /= 10;
        $num = (int)($num / 10);
      }

      $num *= $multiplier;
      return $num;

    } else {
      // dec < 1, round to given decimal digits

      $num_dot_pos = strpos($num, ".");
      if ($num_dot_pos === false) {
        // The number is already an integer
        return $num;
      }

      // find first non-zero decimal digit in the precision, round that many places after the decimal dot
      $dec_dot_pos = strpos($precision, ".");
      if ($dec_dot_pos === false) {
        return false;
      }
      $dec_digits = 1;
      while (substr($precision, $dec_dot_pos + $dec_digits, 1) == "0") {
        $dec_digits++;
      }

      return substr($num, 0, $num_dot_pos + $dec_digits + 1); // +1 because the decimal digit must be counted as well
    }
  }

  /**
   * Take in a range of values [min, max] and calculate what would be bin size, if we would want to
   * summarize the values in a histogram, heatmap or related aggregated structure
   * @param float|string $min
   * @param float|string $max
   * @param float|string $max_bins Maximum number of bins that we are allowed to use
   * @return float Bin size, floating point number, rounded.
   *   Return 0 on error.
   */
  public static function calcBinSize($min, $max, $max_bins) {
    $value_range = $max - $min;
    foreach (self::BIN_SIZES as $bin_size) {
      $n_bins = $value_range / $bin_size;
      if ($n_bins <= $max_bins) {
        return $bin_size;
      }
    }
    return 0;
  }

  /**
   * Calculate pattern matching, using DSP method "Correlation", see http://www.dspguide.com/ch7/3.htm
   * Both signals get normalized - divided by their last value
   * @param array $signal1
   * @param $signal2
   * @return float
   */
  public static function patternMatching($signal1, $signal2) {
    // Scale both signals by their last value
    self::normalize($signal1);
    self::normalize($signal2);

    // We use simplified version here - match the signals only at one position (i = 0)
    $m = count($signal2);
    $sum = 0;
    for ($i = 0; $i < $m; ++$i) {
      $sum += $signal1[$i] * $signal2[$i];
    }
    return $sum;
  }

  /**
   * Normalize the values: center and scale: value = (value - mean) / sigma
   * @param array & $s
   */
  public static function normalize(array &$s) {
    $mean = self::mean($s);
    $n = count($s);
    $sigma = 0;
    for ($i = 0; $i < $n; ++$i) {
      $sigma += ($s[$i] - $mean) ** 2; // (value - mean) squared
    }
    $sigma = sqrt($sigma / $n);
    if ($sigma == 0) {
      $sigma = 1.0; // Avoid division by zero
    }

    for ($i = 0; $i < $n; ++$i) {
      $s[$i] = ($s[$i] - $mean) / $sigma;
    }
  }


  /**
   * Calculate mean of values
   * @param array & $s Array is not changed, reference used only for performance
   * @return float|int
   */
  public static function mean(array &$s) {
    $n = count($s);
    $mean = 0.0;
    for ($i = 0; $i < $n; ++$i) {
      $mean += $s[$i];
    }

    return $mean / $n;
  }

  /**
   * Check if a number is close to a round boundary. If it is, round it a bit up so that it is above the boundary (or round down, if $round_up is false)
   * Note - rounding will happen only if the number is on the other side of the boundary!
   *
   * @param string $number
   * @param bool $round_up When true, the number will be rounded so that it is above the boundary. When false: below
   * the boundary.
   * @param string $max_discance Max distance from rounded boundary. Whenever number is within this distance, it will be rounded.
   * @param float $delta
   * @return string String decimal. Rounded number (or original number if it was not close to the boundary)
   */
  public static function roundCloseToBoundary(string $number, bool $round_up, string $max_discance = self::BOUNDARY_MAX_DISTANCE, $delta = self::BOUNDARY_ROUNDING_DELTA) {
    $boundary = self::getClosestBoundary($number, $max_discance, true);
    if ($boundary !== false) {
      if ($round_up) {
        if (bccomp($number, $boundary) == -1) {
          // Round up only when the number is below boundary
          $number = bcadd($boundary, $delta);
        }
      } else {
        if (bccomp($number, $boundary) == 1) {
          // Round down only when the number is above the boundary
          $number = bcsub($boundary, $delta);
        }
      }
    }
    return $number;
  }

  public static function getClosestBoundary(string $number, string $max_distance, bool $consider_half = false) {
    $rounded = self::_getCloseRoundNumber($number, $max_distance);
    $sign = bccomp($number, 0) >= 0 ? 1.0 : -1.0;
    $dist = bcmul($max_distance, $sign);
    if ($rounded === false) {
      // Maybe the number is a bit too low
      $number = bcadd($number, $dist);
      $rounded = self::_getCloseRoundNumber($number, $max_distance);
      if ($rounded === false && $consider_half) {
        $number = bcsub($number, $dist);
        $number = bcadd($number, 0.5);
        $rounded = self::_getCloseRoundNumber($number, $max_distance);
        if ($rounded === false) {
          $number = bcadd($number, $dist);
          $rounded = self::_getCloseRoundNumber($number, $max_distance);
        }
        if ($rounded !== false) {
          $rounded = bcsub($rounded, 0.5); // Remove the 0.5 we added first
        }
      }
    }
    return $rounded;
  }

  private static function _getCloseRoundNumber(string $number, string $max_distance) {
    // Make sure we get a positive number to process
    $sign = bccomp($number, 0) >= 0 ? 1.0 : -1.0;
    $number = bcmul($number, $sign);

    $round_part = self::roundToDecimal($number, "1.0");
    $remainder = bcsub($number, $round_part);
    $rounded = false;
    if (bccomp($remainder, $max_distance) == -1) {
      $rounded = $round_part;
    }

    // If we had a negative number, make it negative again
    if ($rounded) {
      $rounded = bcmul($rounded, $sign);
    }

    return $rounded;
  }

}