<?php

////////////////////////////////////////////////////////////
/// Explore RAW data gathered from SimplyWall.St API
////////////////////////////////////////////////////////////


use Db\SimplyWstRepository;

require_once("../src/platform_config.php");

// Top 50 buy examples
printFinancialsFor("OB:ENTRA");
printFinancialsFor("OB:KID");
printFinancialsFor("OB:NEL");

// Top 50 sell examples
printFinancialsFor("OB:JIN");
printFinancialsFor("OB:PRS");
printFinancialsFor("OB:ICE");

function printFinancialsFor($symbol) {
  $repo = SimplyWstRepository::getInstance();
  $raw_data = $repo->getRawData($symbol);

  if ($raw_data && isset($raw_data->data->analysis->data)) {
    $analysis = $raw_data->data->analysis->data;
    $ext_analysis = $analysis->extended->data->analysis->past;
    $val_analysis = $analysis->extended->data->analysis->value;
    $pe = $analysis->pe;
    $roa = $analysis->roa;
    $roe = $analysis->roe;
    $roce = $ext_analysis->return_on_capital_employed * 100;
    $psr = $val_analysis->price_to_sales;

    echo "$symbol:\n";
    echo "    P/E: $pe\n    ROA: $roa\n    ROE: $roe\n    ROCE: $roce\n    PSR: $psr\n";
  }
}