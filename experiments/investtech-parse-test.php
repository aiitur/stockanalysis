<?php

////////////////////////////////////////////////////////////////////////////////
// APP: Parse a cached HTML file, extract Top 50 table from it
////////////////////////////////////////////////////////////////////////////////

use Crawlers\InvestTechTableParser;
use Db\InvestTechTop50Repository;

require_once("../src/platform_config.php");

$parser = new InvestTechTableParser();
$html = file_get_contents("../src/apps/debug_output/sell_2019-12-22.html");
$date = new DateTime("2019-12-20 20:00");
$table = $parser->parse($html, $date);

if ($table->getSize() == 50) {
  echo "All 50 stocks retrieved!\n";
  $dbTable = InvestTechTop50Repository::getInstance();
  if ($dbTable->save($table, false)) {
    echo "Entries saved in DB!\n";
  } else {
    echo "Error while saving entries to DB";
  }
}
