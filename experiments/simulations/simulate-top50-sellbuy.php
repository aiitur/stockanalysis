<?php

// Simulate a strategy of buying a stock when it goes from sell-list to buy list in InvestTech Top50 tables

use Db\InvestTechTop50Repository;
use Db\PriceRepository;
use Services\InvestTechService;

require_once("../../src/platform_config.php");

$repo = InvestTechTop50Repository::getInstance();
$sell_stocks = $repo->getAll(false);
$buy_stocks = $repo->getAll(true);
if (!$buy_stocks || !$sell_stocks) {
  echo "Could not get buy and sell lists\n";
  return;
}

// Find stocks which were in the sell list and then moved to the buy list.
// Find the first date they went to buy list.
// Then pretend we buy the stock next day at open price and sell when one of the conditions is true:
// a) It reaches X% target
// b) It reaches Y% S/L
// c) D Days have passed and no threshold is reached
// d) Stock appears in the sell list again - sell at close price
// Pessimistic assumption: if a day has a long candle, both low reaches the S/L and high price reaches target, assume that S/L came first

$sell_to_buy = InvestTechService::getListIntervals($sell_stocks, $buy_stocks);
$price_repo = PriceRepository::getInstance();

foreach ($sell_to_buy as $ticker => $data) {
  $prices = $price_repo->getAllFor($ticker);
  if ($prices) {
    $actions = getOrders($data["stock"], $data["intervals"], $prices);
    if ($actions && count($actions) >= 2) {
      $buy_price = null;
      $buy_date = "";
      $stock_name = $data["stock"];
      foreach ($actions as $a) {
        if ($a["type"] == "buy") {
          $buy_date = $a["date"];
          $buy_price = $a["price"];
        } else if ($a["type"] == "sell") {
          $sell_price = $a["price"];
          $sell_date = $a["date"];
          $profit_percent = round((($sell_price / $buy_price) - 1.0) * 100, 1);
          echo "Bought $stock_name at $buy_price [$buy_date], sold at $sell_price [$sell_date], profit: {$profit_percent}%\n";
        }
      }
    }
  }
}

function getOrders($stock_name, array &$intervals, array &$prices) {
  $actions = [];
  $state = "wait";
  foreach ($intervals as $int) {
    switch ($state) {
      case "wait":
        if ($int["list"] == 1) {
          // Appeared in Sell list
          $state = "sell";
          $last_sell_date = end($int["dates"]);
        }
        break;
      case "sell":
        if ($int["list"] == 2) {
          // Was in sell list, appeared in buy list
          $state = "buy";
          $first_buy_date = reset($int["dates"]);
          $buy_date = getNextPriceDate($first_buy_date, $prices);
          if ($buy_date) {
            $buy_price = $prices[$buy_date]->open;
            $actions[] = [
              "type" => "buy",
              "price" => $buy_price,
              "date" => $buy_date
            ];
          }
        }
        break;
      case "buy":
        if ($int["list"] == 1) {
          // Appeared in Sell list again - sell
          $state = "sell";
          $first_sell_date = reset($int["dates"]);
          $sell_date = getNextPriceDate($first_sell_date, $prices);
          if ($sell_date) {
            $sell_price = $prices[$sell_date]->open;
            $actions[] = [
              "type" => "sell",
              "price" => $sell_price,
              "date" => $sell_date
            ];
          }
          $last_sell_date = end($int["dates"]);
        }
        break;
    }
  }
  return $actions;
}

function getNextPriceDate(string $prev_date, array &$prices) {
  // Code adapted from https://stackoverflow.com/questions/7454904/next-key-in-array
  $price_dates = array_keys($prices);
  $next_date = null;
  while ($next_date === null && ($n = next($price_dates)) !== null) {
    if ($n == $prev_date) {
      $next_date = next($price_dates);
    }
  }
  return $next_date;
}