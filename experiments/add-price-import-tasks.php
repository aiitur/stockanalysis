<?php

/////////////////////////////////////////////////////////////////////////////////
/// Add tasks of importing candles from Yahoo finance for all InvestTech and magic formula top stocks
/////////////////////////////////////////////////////////////////////////////////

use Db\StockRepository;
use Db\TaskRepository;
use Model\StockData;

require_once("../src/platform_config.php");

////////////////////////////////////////////
// Get InvestTech stock raw data
////////////////////////////////////////////
$stockRepo = StockRepository::getInstance();
$buy_stocks = $stockRepo->getInvestTechTopStocks(true);
$sell_stocks = $stockRepo->getInvestTechTopStocks(false);
// Remove duplicate - stocks who have been in both lists: top buys and top sells
$stocks = array_merge($buy_stocks, $sell_stocks);
createPriceImportTasks($stocks);
////////////////////////////////////////////


////////////////////////////////////////////
// Get Magic formula stock raw data
////////////////////////////////////////////
$stocks = $stockRepo->getMagicFormulaTopStocks();
createPriceImportTasks($stocks);
////////////////////////////////////////////

/**
 * Add raw data crawl tasks to the queue for the given stocks. Ignore those who have a recent raw data entry in the DB already.
 * @param array $stocks
 */
function createPriceImportTasks(array &$stocks) {
  echo count($stocks) . " stocks will be added to crawling tasks\n";

  $taskRepo = TaskRepository::getInstance();
  /** @var StockData $stock */
  foreach ($stocks as $stock) {
    if ($stock->yahoo_ticker) {
      $taskRepo->add("PriceImportRunner", 5 * 60, $stock->exchange_symbol);
    } else {
      echo "No Yahoo ticker for $stock->ticker \n";
    }
  }
}