<?php

// Testing SimplyWall.St stock detail crawling task

use Tasks\SWStStockCrawlRunner;

require_once("../src/platform_config.php");

$runner = new SWStStockCrawlRunner();
$runner->start("OB:KID");