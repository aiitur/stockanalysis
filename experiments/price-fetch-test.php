<?php

use Tasks\PriceImportRunner;

require_once("../src/platform_config.php");

$runner = new PriceImportRunner();
$runner->start("OB:PARB");