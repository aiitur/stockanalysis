<?php

////////////////////////////////////////////////////////////////////////////////
// APP: Parse a cached HTML file, extract Top 50 table from it
////////////////////////////////////////////////////////////////////////////////

require_once("../src/platform_config.php");
require_once("../src/MagicFormulaRepository.php");
require_once("../src/MagicFormulaTableParser.php");

$parser = new MagicFormulaTableParser();
$html = file_get_contents("../src/apps/debug_output/magic_formula_2019-12-26.html");
$date = new DateTime();
$table = $parser->parse($html, $date);

if ($table->getSize() == 50) {
  echo "All 50 stocks retrieved!\n";
  $repo = new MagicFormulaRepository();
  if ($repo->save($table)) {
    echo "Entries saved in DB!\n";
  } else {
    echo "Error while saving entries to DB";
  }
}
