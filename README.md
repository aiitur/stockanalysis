# stockAnalysis

To run the application in production:
1) Copy file docker-compose.prod.template.yml to docker-compose.prod.yml - insert MySQL root password
2) Create `src/platform_config_prod.php` file with production-environment config. Use `src/platform_config_tamplete.php`
as a template.
3) If your development config is different, create a dev config in the same fashion, name it `src/platform_config_dev.php`. 
Warning: it is assumed that your DEV machine is Windows!
2) Run the following commands in this folder (use the start-containers.bat file):
    docker-compose -f docker-compose.yml -f docker-compose.prod.yml build 
    docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d
