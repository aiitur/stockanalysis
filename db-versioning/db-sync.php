<?php
////////////////////////////////////////////////////
// A script used to synchronize database state
////////////////////////////////////////////////////
require_once("../src/platform_config.php");

$DB_CHANGELOG_FILE = "db-changelogs.yml";
$JAVA_LIB_DIR = "lib";
$HELP_COMMANDS = ["h", "help", "-h", "--help"];

if ($argc < 2 || in_array($argv[1], $HELP_COMMANDS)) {
    echo "Must pass command! Available commands:\n";
    echo "  update [debug] - Update the DB to the latest version.\n";
    echo "  rollback <tag> [debug] - Roll back DB to version with specified tag\n";
    echo "  " . join(" OR ", $HELP_COMMANDS) . " - Show this help";
    echo "\n";
    echo "When word 'debug' is specified, SQL is not executed, just prited to STDOUT\n";
    return;
}

$driver = "--driver=com.mysql.jdbc.Driver";
$classes = "-cp \"${JAVA_LIB_DIR}/*\" liquibase.integration.commandline.Main";
$user = "--username=" . DB_USER;
$pwd = "--password=" . DB_PWD;
$url = "--url=jdbc:mysql://" . DB_HOST . ":" . DB_PORT . "/" . DB_NAME;
$file = "--changeLogFile={$DB_CHANGELOG_FILE}";
$args = "$driver $user $pwd $url $file";
// Don't echo username and pwd
$public_args = "$driver --username=" . DB_USER . " --password=*** $url $file";

// Check if "debug" parameter is passed
if (in_array("debug", $argv)) {
    // Debug mode on, execute updateSql, rollbackSql, etc
    $cmdSuffix = "Sql";
} else {
    $cmdSuffix = "";
}

if ($argv[1] == "update") {
    echo "Updating db...\n";
    $command = "update{$cmdSuffix}";
} else if ($argv[1] == "rollback") {
    if ($argc < 3 || !$argv[2] || $argv[2] == "debug") {
        echo "Tag is missing. Rollback syntax: rollback <targetDbTag> [debug]\n";
        return;
    }
    $tag = $argv[2];
    echo "Rolling back DB to version tagged with $tag\n";
    $command = "rollback{$cmdSuffix} $tag";
}

$cmdLine = "java $classes $args $command";
$publicCmd = str_replace($args, $public_args, $cmdLine);
echo $publicCmd . "\n";
$output = [];
$ret_val = exec($cmdLine, $output);
echo "Process exited with result: $ret_val\n";
echo "Process output:\n";
foreach ($output as $row) {
    echo "$row\n";
}
